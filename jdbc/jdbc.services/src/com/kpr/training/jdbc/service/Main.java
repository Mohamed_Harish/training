package com.kpr.training.jdbc.service;

import java.util.ArrayList;
//import java.sql.Date;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

public class Main {

	public static void main(String[] args) {
	    
	    //PersonService service = new PersonService();
	    AddressService adService = new AddressService();
	    //ConnectionService serv = new ConnectionService();
	    //Person person = new Person("bharath", "waj", "waj@gmail.com", java.sql.Date.valueOf("1998-08-16"));
	    Address address = new Address("Mehta street", "chennai", 600031);
	    ConnectionService.get();
	    adService.create(address);
	    //ConnectionService.release();
	    //person.setId(4);
	    //person.setAddress(address);
	    
	    //service.readCsvFile("data.csv");
	    //System.out.println(service.readAll());
	    ConnectionService.commitAndRollback(true);
	    ConnectionService.release();
	    
	    

	    //		System.out.println(service.search("chennai 628400"));
//		Address address1 = new Address("sdfdsf", "dsfdsfdf", 83748923);
//		address1.setId(2);
//		
//		PersonService pService = new PersonService();
//		SimpleDateFormat s = new SimpleDateFormat("dd-mm-yyyy");
//		java.sql.Date.valueOf(date)
//		Person person = new Person("ashok", "kspfrgrhamidnln.com", Date.valueOf("2001-09-02"));
//		person.setAddress(address1);
//////		person.setId(3);
//		long id = pService.create(person);
//		System.out.println(id);
//////		pService.update(person);
//	    service.update(address1);
////	    ConnectionService.commit();
//	    String s = null;
//	    int size = 0;
//        try (PreparedStatement ps = ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_TABLE_SIZE)) {
//            ResultSet result = ps.executeQuery();
//            if (result.next()) {
//                size = result.getInt("COUNT(*)");
//            }
//        }catch (Exception e) {
//            throw new AppException(ErrorCode.FAILED_TO_GET_ADDRESS_SIZE, e);
//        }
//        System.out.println(size);
//        System.out.println(s);
//	    service.delete(100);
//	    ConnectionService.commit();
	}

}
/*
 *     
//    public long checkUniqueAddress(Address address, Connection con) {
//        ResultSet result;
//        long id = 0;
//
//        try (PreparedStatement ps = con.prepareStatement(QueryStatement.addressUnique)) {
//
//            ps.setString(1, address.getStreet());
//            ps.setString(2, address.getCity());
//            ps.setLong(3, address.getPostalCode());
//            result = ps.executeQuery();
//            if (result.next()) {
//                id = result.getLong("id");
//            }
//
//        } catch (Exception e) {
//            throw new AppException(ErrorCode.FAILED_TO_CHECK_ADDRESS, e);
//        }
//        return id;
//    }
 */
