/*
 * Requirement: To perform the CRUD operation of the Address.
 * 
 * Entity: 1.Address 2.AddressService 3.AppException 4.ErrorCode
 * 
 * Function declaration: public long create(Address address) {} public Address read(long id) {}
 * public ArrayList<Address> readAll() {} public void update(Address address) {} public void
 * delete(long id) {} public ArrayList<Address> search(String street, String city, String
 * postalCode) {} public Address readAddress(ResultSet result) {} Jobs To Be Done: 1. Create a
 * Address. 2. Read a record in the Address. 3. Read all the record in the addresses. 4. Update an
 * Address. 5. Delete an Address. 6. Search the address which contains given data. 7. Perform common
 * operations for read and readAll.
 */

package com.kpr.training.jdbc.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;

public class AddressService {
    
    private Address address;

    public long create(Address address) {
        
        validateAddress(address);
        try (PreparedStatement ps = ConnectionService.get().prepareStatement(
                QueryStatement.CREATE_ADDRESS_QUERY, PreparedStatement.RETURN_GENERATED_KEYS)) {
            
            preparePS(ps, address);
            ResultSet result;

            if ((ps.executeUpdate() != 1) || !(result = ps.getGeneratedKeys()).next()) {
                throw new AppException(ErrorCode.ADDRESS_CREATION_FAILED);
            }
            
            return result.getLong("GENERATED_KEY");
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_CREATION_FAILED, e);
        }
    }

    public Address read(long id) {

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.READ_ADDRESS_QUERY)) {

            ps.setLong(1, id);
            ResultSet result;

            if ((result = ps.executeQuery()).next()) {
                return prepareAddress(result);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILED, e);
        }
    }

    public ArrayList<Address> readAll() {

        ArrayList<Address> addresses = new ArrayList<>();

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.READALL_ADDRESS_QUERY)) {

            ResultSet result = ps.executeQuery();
            while (result.next()) {
                addresses.add(prepareAddress(result));
            }

            
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILED, e);
        }
        return addresses;
    }

    public void update(Address address) {

        validateAddress(address);
        
        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY)) {
            
            preparePS(ps,address);
            ps.setLong(4, address.getId());

            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_UPDATION_FAILED, e);
        }
    }

    public void delete(long id) {

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY)) {

            ps.setLong(1, id);

            if (ps.executeUpdate() == 0) {
                throw new AppException(ErrorCode.ADDRESS_DELETION_FAILED);
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.ADDRESS_DELETION_FAILED, e);
        }
    }

    public ArrayList<Address> search(String street, String city, String postalCode) {
        
        ArrayList<Address> addresses = new ArrayList<>();

        try (PreparedStatement ps =
                ConnectionService.get().prepareStatement(QueryStatement.ADDRESS_SEARCH)) {

            ps.setString(1, "%" + street + "%");
            ps.setString(2, "%" + city + "%");
            ps.setString(3, "%" + postalCode + "%");
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                addresses.add(prepareAddress(result));
            }
            return addresses;
        } catch (Exception e) {
            throw new AppException(ErrorCode.SEARCHING_ADDRESS_FAILED, e);
        }
    }

    public Address prepareAddress(ResultSet result) {

        try {

            address = new Address(result.getString(Constant.STREET),
                    result.getString(Constant.CITY), result.getInt(Constant.POSTAL_CODE));
            address.setId(result.getLong(Constant.ID));

            return address;
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESS_FAILED, e);
        }
    }
    
    public void preparePS(PreparedStatement ps, Address address) {
        
        try {
            
            ps.setString(1, address.getStreet());
            ps.setString(2, address.getCity());
            ps.setInt(3, address.getPostalCode());
        } catch (Exception e) {
            throw new AppException(ErrorCode.SETTING_VALUE_FAILED, e);
        }
    }

    public long getAddressIdForAddress(Address address, Connection con) {

        ResultSet result;

        try (PreparedStatement ps = con.prepareStatement(QueryStatement.ADDRESS_UNIQUE)) {

            ps.setString(1, address.getStreet());
            ps.setString(2, address.getCity());
            ps.setLong(3, address.getPostalCode());
            
            if (!((result = ps.executeQuery()).next())) {
                return 0;
            }
            return result.getLong(Constant.ID);
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_ADDRESS, e);
        }
    }
    
    public void validateAddress(Address address) {
        
        if (address.getPostalCode() == 0) {
            throw new AppException(ErrorCode.POSTAL_CODE_ZERO);
        }
    }

}
