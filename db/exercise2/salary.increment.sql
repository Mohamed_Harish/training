UPDATE employee.employee_table
   SET annual_salary=annual_salary+(annual_salary*0.1);

SELECT emp_id
      ,first_name
      ,surname
      ,dob
      ,date_of_joining
      ,annual_salary
      ,department_no
      ,area
  FROM employee.employee_table;