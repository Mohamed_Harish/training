SELECT department_name 
      ,MIN(employee_table.annual_salary) AS least_salary
      ,MAX(employee_table.annual_salary) AS highest_salary
  FROM employee.employee_table
      ,employee.department_table
 WHERE employee_table.department_no=department_table.department_id
 GROUP BY department_table.department_id;