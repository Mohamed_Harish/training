MySQL workbench setup link:
https://www.youtube.com/watch?time_continue=40&v=u96rVINbAUI&feature=emb_title

Reference Link for basics:
http://www.mysqltutorial.org/basic-mysql-tutorial.aspx

Reference Link for Normalization:
https://www.geeksforgeeks.org/database-normalization-normal-forms/

Standards:
https://www.sqlstyle.guide/

Complete 1, 2, 3 and 11

 
Practice questions:
1. Create tables to hold employee(emp_id, first_name, surname, dob, date_of_joining, annual_salary), possible depts: ITDesk, Finance, Engineering, HR, Recruitment, Facility

    employee number must be the primary key in employee table
    department number must be the primary key in department table
    department number is the foreign key in the employee table

2. Write a query to insert at least 5 employees for each department
3. Add the Primary key to employee number column
4. Write a query to increment 10% of every employee salary
5. Find out the highest and least paid in all the department
6. Find out the average salary
7. Prepare an example for self-join
8. Write a query to list out employees from the same area, and from the same department
9. Write a query to find out employee birthday falls on that day
10. Prepare a query to find out fresher(no department allocated employee) in the employee, where no matching records in the department table.
11. Write a query to get employee names and their respective department name


Basic MySQL Tutorial
http://www.mysqltutorial.org

Normal Forms in DBMS - GeeksforGeeks
https://www.geeksforgeeks.org

--------------------------------------------------------------------------------------------------------------------------------------------------------------------

 
Problem Statement:
Fetch the student name, session name and staff name
Try to explore about joins
