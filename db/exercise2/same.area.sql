SELECT employee.first_name
      ,employee.area AS same_area
	  ,department.department_name AS same_dept
  FROM employee.employee_table employee,employee.department_table department 
 WHERE employee.department_no = department.department_id
   AND employee.area IN('coimbatore')
   AND department.department_name IN('ITdesk');