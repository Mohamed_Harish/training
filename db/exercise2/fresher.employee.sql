SELECT first_name
      ,surname
      ,department_no
  FROM employee.employee_table
 WHERE department_no IS NULL;