SELECT emp_id
      ,first_name
      ,surname
      ,dob
      ,date_of_joining
      ,annual_salary
      ,department_no
      ,area
  FROM employee.employee_table
 WHERE DATE_FORMAT(dob, '%d-%m')= DATE_FORMAT(SYSDATE(), '%d-%m');