ALTER TABLE employee.employee_table
  ADD area VARCHAR(45);
  
SELECT emp_id
      ,first_name
      ,surname
      ,dob
      ,date_of_joining
      ,annual_salary
      ,department_no
      ,area
  FROM employee.employee_table;