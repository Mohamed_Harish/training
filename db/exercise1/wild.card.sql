SELECT emp_id
      ,first_name
      ,surname
      ,dob
      ,date_of_joining
      ,annual_salary
      ,department_no
      ,area
  FROM employee.employee_table
WHERE first_name LIKE '[ka]%';

SELECT emp_id
      ,first_name
      ,surname
      ,dob
      ,date_of_joining
      ,annual_salary
      ,department_no
      ,area
  FROM employee.employee_table
WHERE surname LIKE '%ur%';

SELECT book_id
      ,author_name
  FROM library.book_table
WHERE author_name LIKE 'sa_a____n';