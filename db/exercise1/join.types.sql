SELECT employee_table.emp_id
	  ,employee_table.first_name
      ,employee_table.surname
      ,department_table.department_name
  FROM employee.employee_table
 CROSS JOIN employee.department_table;


SELECT client_table.client_id
      ,client_table.client_name
      ,client_table.gender
      ,department_table.department_name
  FROM employee.client_table
  LEFT JOIN employee.department_table
    ON client_table.client_id=department_table.department_id;
    
    
SELECT employee_table.emp_id
	  ,employee_table.first_name
      ,employee_table.surname
      ,department_table.department_name
  FROM employee.employee_table
 RIGHT JOIN employee.department_table
    ON employee_table.emp_id=department_table.department_id;
    
    
SELECT employee_table.emp_id
      ,employee_table.first_name
      ,employee_table.surname
      ,department_table.department_name
  FROM employee.employee_table
  LEFT JOIN employee.department_table
    ON employee_table.emp_id=department_table.department_id
 UNION ALL
SELECT employee_table.emp_id
      ,employee_table.first_name
      ,employee_table.surname
      ,department_table.department_name
  FROM employee.employee_table
 RIGHT JOIN employee.department_table
    ON employee_table.emp_id=department_table.department_id;   
    
    
SELECT employee_table.emp_id
      ,employee_table.first_name
      ,employee_table.surname
      ,client_table.client_name
  FROM employee.employee_table
 INNER JOIN employee.client_table
    ON employee_table.emp_id=client_table.client_id;
 