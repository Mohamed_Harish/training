INSERT INTO library.log_table (student_id, student_name, department, book_name) VALUES (1, 'rahul', 'EEE', 'quantum physics');
INSERT INTO library.log_table (student_id, student_name, department, book_name) VALUES (2, 'ravi', 'ECE', 'organic chemistry');
INSERT INTO library.log_table (student_id, student_name, department, book_name) VALUES (3, 'ram', 'CSE', 'engineering maths');
INSERT INTO library.log_table (student_id, student_name, department, book_name) VALUES (4, 'praveen', 'MECH', 'EVS');
INSERT INTO library.log_table (student_id, student_name, department, book_name) VALUES (5, 'raju', 'EEE', 'circuit theory');
