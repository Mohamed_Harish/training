CREATE VIEW crossjoin AS
SELECT employee_table.emp_id
	  ,employee_table.first_name
      ,employee_table.surname
      ,department_table.department_name
  FROM employee.employee_table
 CROSS JOIN employee.department_table;
 
 SHOW TABLES;

SELECT*FROM crossjoin;