SELECT a.student_id
      ,b.student_name AS Issued_name
      ,b.student_name AS Returned_name
      ,a.department
      ,a.book_name
  FROM library.log_table a
      ,library.log_table b
 WHERE a.student_name=b.student_name;