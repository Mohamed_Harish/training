SELECT employee_table.emp_id
      ,employee_table.first_name
      ,employee_table.department_no
      ,department_table.department_name
      ,client_table.client_id
      ,client_table.client_name
  FROM client_table
 INNER JOIN employee_table ON emp_id=client_table.client_id 
 INNER JOIN department_table ON department_id=client_table.client_id
 ORDER BY department_id;