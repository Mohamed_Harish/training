SELECT emp_id
      ,first_name
      ,surname
      ,dob
      ,date_of_joining
      ,annual_salary
      ,department_no
      ,area
  FROM employee.employee_table
WHERE first_name LIKE 'r%';

SELECT emp_id
      ,first_name
      ,surname
      ,dob
      ,date_of_joining
      ,annual_salary
      ,department_no
      ,area
  FROM employee.employee_table
WHERE surname LIKE '%h';

SELECT student_id
      ,student_name
      ,department
      ,book_name
  FROM library.log_table
WHERE student_name NOT LIKE 'r%';