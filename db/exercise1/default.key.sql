CREATE TABLE library.log_table (
			 student_id INT NOT NULL CHECK ( student_id<=10)
			,student_name VARCHAR(45) NOT NULL DEFAULT 'jagadesh'
            ,department VARCHAR(45) NOT NULL
            ,book_name VARCHAR(45) NOT NULL
            ,PRIMARY KEY (student_id)
            ,INDEX student_id USING BTREE (student_id) VISIBLE)
            
  