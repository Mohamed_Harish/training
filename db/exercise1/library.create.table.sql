CREATE DATABASE library;
   CREATE TABLE library.log_table(
				student_name VARCHAR(45)
                ,student_id INT
                ,book_title VARCHAR(45)
                ,book_author VARCHAR(45));
SHOW COLUMNS FROM library.log_table;
