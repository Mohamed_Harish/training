 INSERT INTO semester_fee(
             cdept_id
			,stud_id
            ,semester
            ,amount
            ,paid_year
            ,paid_status)
 SELECT cdept_id
       ,id AS stud_id
       ,'3' AS semester
       ,'50000' AS amount
       ,NULL AS paid_year
       ,'Unpaid' AS paid_status
   FROM university_detail.student
  ORDER BY student.id