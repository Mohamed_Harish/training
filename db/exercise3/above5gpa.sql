SELECT student.id
	  ,student.roll_number
      ,student.name
      ,student.gender
      ,college.code
      ,college.name
      ,semester_result.grade
      ,semester_result.credits AS GPA
  FROM university_detail.university
      ,university_detail.college
      ,university_detail.student
      ,university_detail.semester_result
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id
   AND semester_result.stud_id = student.id
   AND semester_result.credits>5;