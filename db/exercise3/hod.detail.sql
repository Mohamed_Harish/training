SELECT college.code
      ,college.college_name
      ,university.university_name
      ,college.city
      ,college.state
      ,college.year_opened
      ,department.dept_name
  FROM (SELECT clg.code
              ,clg.name AS college_name
              ,clg.univ_code
              ,clg.city
              ,clg.year_opened
              ,clg.state
              ,emp.name AS HoD_Name
              ,emp.cdept_id
          FROM employee AS emp
               INNER JOIN college AS clg
               ON emp.college_id = clg.id
			   INNER JOIN designation AS desig
			   ON emp.desig_id = desig.id
         WHERE desig.name = 'Head of Department') AS college
         INNER JOIN university AS university
                 ON college.univ_code = university.univ_code
         INNER JOIN college_department AS c_dept
                 ON c_dept.cdept_id = college.cdept_id
         INNER JOIN department
                 ON c_dept.udept_code = department.dept_code
  WHERE department.dept_name IN ('Computer Science Engineering', 'Information Technology');