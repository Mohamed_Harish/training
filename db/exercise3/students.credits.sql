SELECT university.university_name
      ,student.id AS roll_number
	  ,student.name AS student_name
      ,student.gender
      ,student.dob
      ,student.address
      ,college.name AS college_name
	  ,semester_result.semester
      ,semester_result.grade
      ,semester_result.credits
  FROM university_detail.student student
  INNER JOIN university_detail.college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN university_detail.college college
    ON college.id = college_department.college_id
 INNER JOIN university_detail.university university
    ON university.univ_code = college.univ_code
  INNER JOIN university_detail.syllabus
    ON syllabus.cdept_id = college_department.cdept_id
 INNER JOIN university_detail.semester_result semester_result
    ON semester_result.syllabus_id = syllabus.id
  AND semester_result.stud_id = student.id
ORDER BY college.name ASC
        ,semester_result.semester ASC;

    
    