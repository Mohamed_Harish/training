UPDATE university_detail.semester_fee  
   SET semester_fee.paid_year = '2020'
 WHERE stud_id = (SELECT id
                   FROM university_detail.student
				  WHERE roll_number = '108ce010');
UPDATE university_detail.semester_fee  
   SET semester_fee.paid_status = 'paid'
	  ,semester_fee.paid_year = '2019'
 WHERE stud_id IN (SELECT id
                    FROM university_detail.student
				   WHERE roll_number IN ('107ce001'
                                         ,'207ce002'
                                         ,'206cs002'
                                         ,'107ce002'
                                         ,'108ce003'
                                         ,'108ce010'
                                         ,'108ce011'
                                         ,'108ce012'
                                         ,'108ce013'
                                         ,'107cs001'
                                         ,'107cs003'
                                         ,'207ce001'
                                         ,'407ee001'));

SELECT cdept_id
      ,stud_id
      ,semester
      ,amount
      ,paid_year
      ,paid_status
  FROM university_detail.semester_fee;