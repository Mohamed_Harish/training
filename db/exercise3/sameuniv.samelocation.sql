SELECT student.roll_number AS Roll_Number
      ,student.name AS Student_Name
      ,student.gender AS Gender
      ,student.dob AS DoB 
      ,student.email
      ,student.phone
      ,college.name AS College_Name
      ,college.city AS College_city
      ,department.dept_name
      ,employee.name AS HoD_Name
      ,university.university_name
  FROM university_detail.student student
  INNER JOIN university_detail.college_department
    ON student.cdept_id = college_department.cdept_id
 INNER JOIN university_detail.department
    ON department.dept_code = college_department.udept_code
 INNER JOIN university_detail.college
    ON college.id = college_department.college_id
 INNER JOIN university_detail.university
    ON college.univ_code = university.univ_code
 INNER join university_detail.employee
    ON employee.cdept_id = college_department.cdept_id
   AND employee.college_id = college.id
    WHERE university.university_name IN ('Anna University')
   AND college.city IN ( 'Coimbatore')
   ANd employee.desig_id = (SELECT designation.id
                              FROM university_detail.designation
							 WHERE designation.name = 'Head of Department')
 LIMIT 20;
 