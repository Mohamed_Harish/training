SELECT employee.name AS employee_name
      ,university.university_name
      ,college.name AS college_name
      ,department.dept_name
      ,designation.name AS designation_name
      ,designation.rank AS employee_rank
  FROM university_detail.employee employee
 INNER JOIN university_detail.designation
    ON employee.desig_id = designation.id
 INNER JOIN university_detail.college_department
    ON employee.cdept_id = college_department.cdept_id
 INNER JOIN university_detail.college
    ON college_department.college_id = college.id
 INNER JOIN university_detail.university
    ON college.univ_code = university.univ_code
 INNER JOIN university_detail.department
    ON college_department.udept_code = department.dept_code
 WHERE college.univ_code = (SELECT university.univ_code
                              FROM university_detail.university university
							 WHERE university.university_name = 'Anna University')
  
 ORDER BY college.name , designation.rank; 