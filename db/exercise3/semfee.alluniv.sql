SELECT semester
      ,paid_status
      ,SUM(amount) AS Total_Amount
  FROM university_detail.semester_fee
 GROUP BY semester 
        ,paid_status;