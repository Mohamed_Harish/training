/*
Requirement:
    To Write a Java program to copy all of the mappings from the specified map to another map.

Entity:
	CopyMapDemo
	
Function Declaration:
	public static void main(String[] args)
	
Jobs to be done:
	1. Create a hashmap instance as hashMAp1.
	    1.1 Add elements to those instances.
	    1.2 Print the elements.
	2. create an another instance as hashMap2
	    2.1 Add elements to hashMap2.
	    2.2 Print hashhMap2.
    3.Copy elements of hashMap1 to hashMap2.
Pseudo Code:
class CopyMapDemo {

    public static void main(String[] args) {
        HashMap<Integer, String> hashMap1 = new HashMap<>();
        hashMap1.put(key,value);
        System.out.println(hashMap1);

        HashMap<Integer, String> hashMap2 = new HashMap<>();
        hashMap2.put(key,value);
        System.out.println(hashMap2);

        hashMap2.putAll(hashMap1);
        System.out.println(hashMap2);
    }
}

 */
package com.java.training.map;

import java.util.HashMap;

public class CopyMapDemo {

    public static void main(String[] args) {
        HashMap<Integer, String> hashMap1 = new HashMap<>();
        hashMap1.put(1, "Red");
        hashMap1.put(2, "Green");
        hashMap1.put(3, "Black");
        System.out.println("The values of hashMap1 is" + " " + hashMap1);

        HashMap<Integer, String> hashMap2 = new HashMap<>();
        hashMap2.put(4, "White");
        hashMap2.put(6, "Blue");
        hashMap2.put(5, "Orange");
        System.out.println("the value of hashMap2 is" + " " + hashMap2);

        hashMap2.putAll(hashMap1);
        System.out.println("the value of hashMap is" + " " + hashMap2);
    }
}
