/*
Requirement:
    To write the java program to count size of mappings.

Entity:
    SizeMappingDemo

Function Declaration:
    public static void main(String[] args)
    size()

Jobs to be done:
    1. Create a new instance of hashmap.
    2. Add the elements to that map.
    3. Print the hashMap.
    4. Print the size of the map.

Pseudo code:
class SizeMappingDemo {

    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(key,value);
        System.out.println(hashMap);
        System.out.println(hashMap.size());
    }
}

*/
package com.java.training.map;

import java.util.HashMap;

public class SizeMappingDemo {

    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "Harish");
        hashMap.put(2, "Ajay");
        hashMap.put(3, "Aniruth");
        hashMap.put(4, "Vijay");
        hashMap.put(5, "Sethupathy");
        hashMap.put(6, "Lawrance");
        hashMap.put(8, "Sugan");
        hashMap.put(7, "Balaji");
        System.out.println("the keys and values are " + hashMap);
        System.out.println("the count of the size of the map is " + hashMap.size());
    }
}
