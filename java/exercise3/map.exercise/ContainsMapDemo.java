/*
Requirement:
    To write a Java program to test if a map contains a mapping for the specified key.

Entity:
    public class ContainsMapDemo.
    
Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1. Create a hashmap instance as hashMap.
    2. Add elements to it.
    3. Check if a value exist in the hashMap.
    
Pseudo code:
class ContainsMapDemo {

    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        //add key, value pair
        hashMap.put(key,value);
        boolean value = hashMap.containsKey(11);
        System.out.println(value);

        boolean value1 = hashMap.containsKey(12);
        System.out.println(value1);
    }
}
*/
package com.java.training.map;

import java.util.HashMap;

public class ContainsMapDemo {

    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(10, "Surya");
        hashMap.put(25, "Vijay");
        hashMap.put(20, "Abisheak");
        hashMap.put(76, "James");
        hashMap.put(12, "Michael");
        boolean value = hashMap.containsKey(11);
        System.out.println("if the key 11 presents in hashMap" + " " + value);

        boolean value1 = hashMap.containsKey(12);
        System.out.println("if the key 12 presents in hashMap" + " " + value1);
    }
}
