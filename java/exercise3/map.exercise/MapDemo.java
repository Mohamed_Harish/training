/*
Requirement:
    To Write a Java program to get the portion of a map whose keys range from a given key to another key.
     
Entity:
    MapDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1. Create an instance of a treemap and add elements to it.
    2. Perform slicing with the map.
Pseudo code:
class MapDemo {

    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(key,value);
        System.out.println(treeMap);
        System.out.println(treeMap.subMap(key1,key2));
    }
}

*/
package com.java.training.map;

import java.util.TreeMap;

public class MapDemo {

    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(10, "Red");
        treeMap.put(20, "Green");
        treeMap.put(30, "Blue");
        treeMap.put(40, "Black");
        treeMap.put(50, "White");
        System.out.println("the treemap is " + treeMap);
        System.out.println("the range is " + treeMap.subMap(20, 50));
    }
}
