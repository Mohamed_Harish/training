/*
Requirement:
    To code the simple lambda expression using the method getValue()

Entity:
    public class SimpleLambdaDemo 

Function Declaration:
    public int getValue()
    public static void main(String[] args)
    
Jobs to be done:
    1. Store value in a variable.
    2. Print the value using getValue method.
    
Pseudo code:
class LambdaDemo {
    
    public static void main(String[] args) {
        SampleDemo value = () -> value;
        System.out.println(value.getValue());
    }
}

*/
package com.java.training.lambda;

interface SampleDemo {
    
    public int getValue();
}

public class LambdaDemo {
    
    public static void main(String[] args) {
        SampleDemo value = () -> 100;
        System.out.println("The value is: " + value.getValue());
    }
}
