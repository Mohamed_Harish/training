/*
Requirement:
    To write a program to print difference of two numbers using lambda expression 
    and the single method interface

Entity:
    NumberDifference

Function Declaration:
    public static void main(String[] args){}

Jobs to be done:
    1. Get the two numbers of specific type from the user.
        1.1 Subtract the two numbers.
    2. Print the difference between the numbers using lambda expression.

Pseudo code:
class NumberDifference {
    
    public static void main(String[] args) {
        Differentiable value = (value1, value2) -> {
            int differenceValue = (value1 - value2);
            return differenceValue;
        };
        
        System.out.println("Difference value : " + value.difference(value1, value2));
    }
}
*/
package com.java.training.lambda;

interface Differentiable {
    
    public int difference(int number1, int number2);
}

public class NumberDifference {
    
    public static void main(String[] args) {
        Differentiable value = (value1, value2) -> {
            int differenceValue = (value1 - value2);
            return differenceValue;
        };
        
        System.out.println("Difference value : " + value.difference(500, 5));
    }
}