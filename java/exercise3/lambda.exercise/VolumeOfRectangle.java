/* 
Requirement:
    To print the volume of the rectangle using Lambda Expressions.
    
Entity:
    public class VolumeOfRectangle.

Function Declaration:
    public double printVolume(double length, double width, double height)
    public static void main(String[] args)

Jobs to be done:
    1. Declare the length, width, height.
    2. Multiply the length, width and height.
        2.1 Print as volume of the rectangle.

Pseudo code:
class VolumeOfRectangle {
    
    public static void main(String[] args) {
        Rectangle volume = (length, width, height) -> {
            double volumeOfRectangle = length * width * height;
            return (volumeOfRectangle);
        };
        System.out.println(volume.printVolume(length, width, height));
    }
}
        
*/
package com.java.training.lambda;

interface Rectangle {
    
    public double printVolume(double length, double width, double height);
}

public class VolumeOfRectangle {
    
    public static void main(String[] args) {
        Rectangle volume = (length, width, height) -> {
            double volumeOfRectangle = length * width * height;
            return (volumeOfRectangle);
        };
        System.out.println("volume of the rectangle : " + volume.printVolume(4, 2, 6));
    }
}
        