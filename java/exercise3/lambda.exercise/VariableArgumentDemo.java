/*
Requirement:
    To print the sum of the variable arguments.

Entity:
    VariableArguementDemo

Function declaration:
    public int printAddition(int ... numbers)
    public static void main(String[] args)
    
Jobs to be done:
    1. Assign n number of integer input.
    2. Declare sum as zero.
    3. Add the each number to the sum.
        3.1 Store the value in sum.
    4. Print the sum.
    
Pseudo code:
class VariableArgumentDemo {
    
    public static void main(String[] args) {
        VariableArgument value = (numbers) -> {
            int sum = 0;
            
            for (int values : numbers) {
                sum = sum + values;
            }
            
            return sum;
        };
        
        System.out.println(value.printAddition(value1, value2, value3));
    }
}

*/
package com.java.training.lambda;

interface VariableArgument {
    
    public int printAddition(int ... numbers);
}

public class VariableArgumentDemo {
    
    public static void main(String[] args) {
        VariableArgument value = (numbers) -> {
            int sum = 0;
            
            for (int values : numbers) {
                sum = sum + values;
            }
            
            return sum;
        };
        
        System.out.println("The sum of elements is: " + value.printAddition(1, 2, 3));
    }
}
