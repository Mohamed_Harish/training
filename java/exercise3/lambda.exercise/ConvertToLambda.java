/*
Requirement:
    To convert the given Anonymous class to Lambda expression.
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}

Entity:
    ConvertToLambda

Function Declaration:
    public boolean isEven(int value)
    public static void main(String[] args)

Jobs to be done:
    1. Declare a value .
    2. Check if the value divided by zero gives zero.
        2.1 Print true.
       
Pseudo code:
class ConvertToLambda {
    
    public static void main(String[] args) {
        CheckNumber evenOrOdd = (value) -> {
            
            if (value %2 == 0) {
                return true;
            } else {
                return false;
            }
        };
        
        System.out.println(evenOrOdd.isEven(value));
    }

}

*/
package com.java.training.lambda;

interface CheckNumber {
    
    public boolean isEven(int value);
}

public class ConvertToLambda {
    
    public static void main(String[] args) {
        CheckNumber evenOrOdd = (value) -> {
            
            if (value %2 == 0) {
                return true;
            } else {
                return false;
            }
        };
        
        System.out.println("The statement that the number is even is " + evenOrOdd.isEven(110));
    }

}
