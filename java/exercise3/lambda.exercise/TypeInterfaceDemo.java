/*
Requirement:
    To find the error line and to fix it using BiFunction Interface for the program.
    public interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
        return number1 + number2;
        };
        
        int print = function.print(int 23,int 32);
        
        System.out.println(print);
    }
}

Entity:
    TypeInterfaceDemo
    
Jobs to be done:
    1. Check the given code snippet.
    2. Identify the error.
    3. Add the given two numbers.
        3.1 Print the addition of two numbers.
        
Pseudo code:
class TypeInterfaceDemo {
    
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
            return number1 + number2;
        };        
        
        System.out.println(function.print(number1, number2));
    }

}
*/
package com.java.training.lambda;

interface BiFunction {
    int print(int number1, int number2);
}

public class TypeInterfaceDemo {
    
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
            return number1 + number2;
        };        
        
        System.out.println(function.print(23, 32));
    }

}
