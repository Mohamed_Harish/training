/*
Requirement:
    To write a Lambda expression program with a single method interface to concatenate two strings.

Entity:
    ConCatDemo.

Function Declaration:
    public String conCat(String firstString, String secondString)
    public static void main(String[] args)

Jobs to be done:
    1. Declare two string values.
    2. Concate the two string values using lambda expression.
    3. Print the concated string.

Pseudo code:
class ConcatDemo {
    
    public static void main(String[] args) {
        ConcationDemo conCatString = (firstString, secondString) -> (firstString + secondString);
        System.out.println(conCatString.conCat(string1, string2));
    }
}

*/
package com.java.training.lambda;

interface ConcationDemo {
    public String conCat(String firstString, String secondString);
}

public class ConcatDemo {
    
    public static void main(String[] args) {
        ConcationDemo conCatString = (firstString, secondString) -> (firstString + secondString);
        System.out.println(conCatString.conCat("Mohamed", "Harish"));
    }
}
