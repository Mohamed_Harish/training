/*
Requirements : 
	Reverse List Using Stack with minimum 7 elements in list.

Entities :
	public class ReverseList.

Function Declaration :
	public static void main(String[] args)

Jobs To Be Done:
	1. Create a array list of type integer. 
        1.1 Add some elements.
        1.2 Print the list.
	2. Create a stack 
	    2.1 Add the elements of the list to it.
	3.Clear the list.
	4.Add the elements to the empty list from stack by pop
	    4.1 print the list.
	
Pseudo code:
class ReverseList {
    public static void main(String[] args) {
        ArrayList<type> list = new ArrayList<>();
        // add elements
        list.add(elements);
        print list;
        
        Stack<type> stack = new Stack<>();  //create a stack
        stack.push(elements);
        
        list.clear();
        //for the size of stack
        list.add(stack.pop())
        print list;
    }
}
        
*/
package com.java.training.stackandqueue;

import java.util.ArrayList;
import java.util.Stack;

public class ReverseList {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(5);
        list.add(4);
        list.add(3);
        list.add(2);
        list.add(1);
        System.out.println("Before Reversing : " + list);

        Stack<Integer> stack = new Stack<>();
        for (Integer value : list) {
            stack.push(value);
        }
        list.clear();
        int size = stack.size();
        for (int iteration = 0; iteration < size; iteration++) {
            list.add(stack.pop());
        }
        System.out.println("After Reversing : " + list);

    }
}
