/*
Requirement:
    To complete the following code and to find its output.
     Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());   

Entity:
    PriorityQueueDemo.

Function Declaration:
    public static void main(String[] args)
    poll(), peak().

Jobs to be Done:
    1. Create a priority queue and add the elements.
    2. Perform poll and peek method to it.
    
Pseudo code:
class PriorityQueueDemo {
    public static void main(String[] args) {
         Queue<type> bike = new PriorityQueue<>();
         //add elements to it 
         bike.add(element);
         //use poll() method
         //print peek
         // print the queue
    }
}
*/
package com.java.training.stackandqueue;

import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueDemo {

    public static void main(String[] args) {
        Queue<String> bike = new PriorityQueue<>();
        bike.add("Shine");
        bike.add("CBR 120");
        bike.add("FZ");
        bike.add("R15");
        bike.poll();
        System.out.println(bike.peek());
        System.out.println(bike);
    }

}
