/*
Requirement:
	1. To write a program to print employees name list by implementing iterable interface.
    2. To state why except iterator() method in iterable interface, are not neccessary to define 
    in the implemented class?

Entity:
	public class IterableDemo
	
Function declaration:
	public static void main(String[] args)
	iterator()
	checkMethod()
	
Jobs to be done:
	1. Create an array list instance.
	    1.1 Add employees name as its elements.
    2. Create an iterator for the array list.
        2.1 Print the employee names by forEach method.
        2.2 Print the employee names by enhanced for loop.

Pseudo code:
class IterableDemo implements Iterable<String> {

    public ArrayList<String> employeeList;

    public IterableDemo() {
        employeeList = new ArrayList<String>();
    }

    public void addEmployee(String employee) {
        employeeList.add(employee);
    }

    public Iterator<String> iterator() {
        return employeeList.iterator();
    }

    public void checkMethod() {
        employeeList.forEach((member) -> {
            System.out.println(member);
        });
    }

    public static void main(String[] args) {
        IterableDemo employees = new IterableDemo();
        employees.addEmployee(names);
        employees.checkMethod();
        for (String name : employees) {
            System.out.println(name);
        }
    }
}

*/
package com.java.training.generics;

import java.util.ArrayList;
import java.util.Iterator;

public class IterableDemo implements Iterable<String> {

    public ArrayList<String> employeeList;

    public IterableDemo() {
        employeeList = new ArrayList<String>();
    }

    public void addEmployee(String employee) {
        employeeList.add(employee);
    }

    public Iterator<String> iterator() {
        return employeeList.iterator();
    }

    public void checkMethod() {
        System.out.println("Employees Name");
        employeeList.forEach((member) -> {
            System.out.println(member);
        });
    }

    public static void main(String[] args) {
        IterableDemo employees = new IterableDemo();
        employees.addEmployee("Kiran");
        employees.addEmployee("Pavi");
        employees.addEmployee("Harish");
        employees.addEmployee("Dave");
        employees.checkMethod();
        System.out.println("--------");
        System.out.println("Employees Name");
        for (String name : employees) {
            System.out.println(name);
        }
    }
}


/*
Except iterator() method in Iterable interface, all other has default implementation.
Hence it is not neccessary to define other methods.
*/