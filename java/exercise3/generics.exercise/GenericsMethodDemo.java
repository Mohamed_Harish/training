/*
Requirements : 
    Write a generic method to count the number of elements in a collection that have a specific
    property (for example, odd integers, prime numbers, palindromes).

Entities :
    public class CountSpecificProperty.
  
Function Declaration :
    public static void main(String[] args)
  
Jobs To Be Done:
    1. Create an array list instance.
        1.1 Add integers as its elements.
    2. Assign value as 0.
        2.1 Check every element of the list.
            2.1.1 Check if divided by 2 is not equal to zero.
                2.1.1.1 Increase the value by 1.
    3. Print the count of the value.
    
Pseudo code:
class GenericsMethodDemo {

    public static int countOddNumber(ArrayList<Integer> list) {
        int value = 0;
        for (int elements : list) {
            if (elements % 2 != 0) {
                value++;
            }
        }

        return value;
    }

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(elements);
        System.out.println(countOddNumber(list));
    }
}
*/
package com.java.training.generics;

import java.util.ArrayList;

public class GenericsMethodDemo {

    public static int countOddNumber(ArrayList<Integer> list) {
        int value = 0;
        for (int elements : list) {
            if (elements % 2 != 0) {
                value++;
            }
        }

        return value;
    }

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(25);
        list.add(15);
        list.add(22);
        list.add(24);
        list.add(26);
        list.add(27);
        list.add(21);
        list.add(20);
        System.out.println("the count of odd number is " + countOddNumber(list));
    }
}
