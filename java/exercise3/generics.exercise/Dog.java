/*
Requirements : 
    To write a program to demonstrate generics - class objects as type literals.
 
Entities :
    public class Dog implements Animal
     
Function Declaration :
    public static <T> boolean checkInterface(Class<?> theClass)
    public void sound()
    public static void main(String[] args)
     
Jobs To Be Done:
    1. Create a animal interface.
        1.1 Declare a method as sound.
    2. Create a dog class.
        2.1 Declare checkInterface method.
        2.2 Define sound method.
    3. Check for the interface for dog and animal.
        3.1 Print the boolean value.
        3.2 Print the class name.
        
Pseudo code:
interface Animal {
    public void sound();
}


class Dog implements Animal {

    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }

    public void sound() {
        System.out.println("Barking");
    }

    public static void main(String[] args) {
        Class<Integer> intClass = int.class; // String.class,Integer.class,Boolean.class
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1); // prints false
        System.out.println(intClass.getClass()); // prints java.lang.Class
        System.out.println(intClass.getName()); // prints int

        boolean boolean2 = checkInterface(Dog.class);
        System.out.println(boolean2); // prints false
        System.out.println(Dog.class.getClass()); // prints java.lang.Class
        System.out.println(Dog.class.getName()); // prints Dog

        boolean boolean3 = checkInterface(Animal.class);
        System.out.println(boolean3); // prints true
        System.out.println(Animal.class.getClass()); // prints java.lang.Class
        System.out.println(Animal.class.getName()); // prints Animal

        
    }
}
*/
package com.java.training.generics;

interface Animal {
    public void sound();
}


public class Dog implements Animal {

    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }

    public void sound() {
        System.out.println("Barking");
    }

    public static void main(String[] args) {
        Class<Integer> intClass = int.class; // String.class,Integer.class,Boolean.class
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1); // prints false
        System.out.println(intClass.getClass()); // prints java.lang.Class
        System.out.println(intClass.getName()); // prints int

        boolean boolean2 = checkInterface(Dog.class);
        System.out.println(boolean2); // prints false
        System.out.println(Dog.class.getClass()); // prints java.lang.Class
        System.out.println(Dog.class.getName()); // prints Dog

        boolean boolean3 = checkInterface(Animal.class);
        System.out.println(boolean3); // prints true
        System.out.println(Animal.class.getClass()); // prints java.lang.Class
        System.out.println(Animal.class.getName()); // prints Animal

        try {
            Class<?> testClass = Class.forName("Dog");
            System.out.println(testClass.getClass());
            System.out.println(testClass.getName());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }
}
