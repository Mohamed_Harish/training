/*
Requirement:
    To find the output of the below program.
    public class UseGenerics {
        public static void main(String[] args) {  
            MyGen<Integer> m = new MyGen<Integer>();  
            m.set("merit");
            System.out.println(m.get());
        }
    }
    class MyGen<T> {
    
        T var;
    
        void  set(T var) {
            this.var = var;
        }
        T get() {
            return var;
        }
    }
    
Entity:
    public class OutputDemo

Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create an instance for a class of type integer.
    2. Set the value in string type.
    3. Print the value.
    4. Check the output.
    
Pseudo code:
class OutputDemo {

    public static void main(String[] args) {
        MyGen<Integer> m = new MyGen<Integer>();
        // m.set(string); // compile time error
        System.out.println(m.get());
    }
}


class MyGen<T> {

    T var;

    void set(T var) {
        this.var = var;
    }

    T get() {
        return var;
    }
}

*/
package com.java.training.generics;

public class OutputDemo {

    public static void main(String[] args) {
        MyGen<Integer> m = new MyGen<Integer>();
        // m.set("merit"); // compile time error
        System.out.println(m.get());
    }
}


class MyGen<T> {

    T var;

    void set(T var) {
        this.var = var;
    }

    T get() {
        return var;
    }
}

// There will be compile time error 