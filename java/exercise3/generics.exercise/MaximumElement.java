/* 
Requirement:
    To find the maximum element of the given program using generic method.

Entity:
    public class MaximalElement

Function Declaration:
    public static void main(String[] args)
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    
Jobs to be Done:
    1. Create an array instance.
        1.1 Add integer elements to the list.
    2. For the given range
        2.1 Check for the maximum element
        2.2 Print the maximum element.
        
Pseudo code:
class MaximumElement {

    public static <T extends Comparable<T>> T max(List<? extends T> list, int begin, int end) {
        T maximumElement = list.get(begin);
        for (++begin; begin < end; ++begin) {
            if (maximumElement.compareTo(list.get(begin)) < 0) {
                maximumElement = list.get(begin);
            }
        }
        return maximumElement;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(elements);
        System.out.println(max(list, from, to));
    }
}
*/
package com.java.training.generics;

import java.util.List;
import java.util.Arrays;

public class MaximumElement {

    public static <T extends Comparable<T>> T max(List<? extends T> list, int begin, int end) {
        T maximumElement = list.get(begin);
        for (++begin; begin < end; ++begin) {
            if (maximumElement.compareTo(list.get(begin)) < 0) {
                maximumElement = list.get(begin);
            }
        }
        return maximumElement;
    }

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println(max(list, 3, 6));
    }
}
