/*
Requirement:
    To write a generic Method to swap the elements.

Entity:
    public class SwapDemo

Function Declaration:
    public static <T> void swap(T[] list, int firstNumber, int lastNumber)
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber)
    public static void main(String[] args)

Jobs to be done:
    1. Create a list instance.
        1.1 Add elements to the list.
    2. Pass the list , first number and last number to swap method.
        2.1 Store the first number in a temporary variable.
        2.2 Store the last number in first number.
        2.3 Store the temporary variable in last number.
    3. Print the swapped elements.
    
Pseudo code:
class SwapDemo {

    public static <T> void swap(T[] list, int firstNumber, int lastNumber) {
        T temporaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temporaryVariable;
        System.out.println(Arrays.toString(list));
    }

    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber) {
        Collections.<T>swap(list1, firstNumber, secondNumber);
    }

    public static void main(String[] args) {
        Integer[] list = {elements};
        swap(list, first number, last number);

        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        printSwap(list1, first number, last number);
        System.out.println(list1);
    }
}
*/
package com.java.training.generics;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class SwapDemo {

    public static <T> void swap(T[] list, int firstNumber, int lastNumber) {
        T temporaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temporaryVariable;
        System.out.println("The swapped element is " + Arrays.toString(list));
    }

    @SuppressWarnings("unused")
    public static <T> void printSwap(List<T> list1, int firstNumber, int secondNumber) {
        Collections.<T>swap(list1, firstNumber, secondNumber);
    }

    public static void main(String[] args) {
        Integer[] list = {110, 250, 300, 234, 540, 600};
        swap(list, 2, 4);

        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        printSwap(list1, 2, 4);
        System.out.println(list1);
    }
}
