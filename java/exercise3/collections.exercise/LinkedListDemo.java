/*
Requirement:
    To create an Array list of 7 elements and to create an empty linked list and to store the elements
    of array list to linked list and to display it.

Entity:
    LinkedListDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an instance of array list as list.
        1.1 Add elements to it.
    2. Create an instance of linked list.
        2.1 Add elements of list to linked list.
    3. Print the linked list.

Pseudo code:
class LinkedListDemo {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(elements);
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.addAll(list);

        for (int number : linkedList) {
            System.out.print(number);
        }
    }
}

*/
package com.java.training.collections;

import java.util.ArrayList;
import java.util.LinkedList;

public class LinkedListDemo {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);

        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.addAll(list);

        for (int number : linkedList) {
            System.out.print(number + " ");
        }
    }
}
