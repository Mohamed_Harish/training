/*
Requirement:
    create a pattern for password which contains
    8 to 15 characters in length
    Must have at least one uppercase letter
    Must have at least one lower case letter
    Must have at least one digit
Entity:
    public class RegexPassword
Function Declaration:
    public static boolean ValidPassword(String password)
    public static void main(String[] args)
Jobs to Done:
    1. Create a validPassword method of boolean return type.
        1.1 Declare pattern of the password as regex.
        1.2 Check the given password matches the pattern.
            1.2.1 If not matches, returns false
    2. Get user input for password.
    3. Pass the password for checking using validPassword.

Pseudo code:
class RegexPassword {

    public static boolean validPassword(String password) {
        String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";

        Pattern pattern = Pattern.compile(regex);
        if (password == null) {
            return false;
        }

        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String password = scanner.next();
        System.out.println(validPassword(password));
    }
}
   
    
*/
package com.java.training.collections;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexPassword {

    public static boolean validPassword(String password) {
        String regex = "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";

        Pattern pattern = Pattern.compile(regex);
        if (password == null) {
            return false;
        }

        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the Password ");
        String password = scanner.next();
        System.out.println(validPassword(password));
        scanner.close();
    }
}
