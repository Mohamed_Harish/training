/*
Requirement:
    To demonstrate the difference between poll() method and remove() method.

Entity:
    RemoveAndPoll

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1. Create an instance of  priority queue.
        1.1 Add elements to the queue.
    2. Perform poll for the queue.
        2.1 Print the queue
    3. Clear the queue.
        3.1 Print the poll for the queue.
    4. Create a another priority queue instance as string.
        4.1 Add elements to it.
        4.2 perform remove for the string.
    5. Clear the string.
        5.1 Perform remove for the empty string.

Pseudo code:
class RemoveAndPoll {

    public static void main(String[] args) {
        Queue<Integer> queue = new PriorityQueue<>();
        queue.add(elements)
        queue.poll();
        System.out.println(queue);

        queue.clear();
        System.out.println(queue.poll()); // prints null.

        Queue<String> string = new PriorityQueue<>();
        string.add(elements);
        string.remove();
        System.out.println(string);

        string.clear();
        System.out.println(string.remove()); // throws exception
    }
}
*/
package com.java.training.collections;

import java.util.Queue;
import java.util.PriorityQueue;

public class RemoveAndPoll {

    public static void main(String[] args) {
        Queue<Integer> queue = new PriorityQueue<>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        queue.poll();
        System.out.println("After poll " + queue);

        queue.clear();
        System.out.println("After clear " + queue.poll()); // prints null.

        Queue<String> string = new PriorityQueue<>();
        string.add("Hii");
        string.add("Program");
        string.add("java");
        string.add("password");
        string.add("Enter a valid password");
        string.add("Error");
        string.remove();
        System.out.println("After remove method " + string);

        string.clear();
        System.out.println("After the clear method " + string.remove()); // throws exception
    }
}

/* 
From this example the poll() is used to remove or retrieve the top element and when the queue is empty
it returns the null but remove() throws exception.
*/