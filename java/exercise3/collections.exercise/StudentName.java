/*
Requirements:
    LIST CONTAINS 10 STUDENT NAMESkrishnan, abishek, arun,vignesh, kiruthiga, murugan,adhithya,balaji,vicky, priya and 
    display only names starting with 'A'.
    
Entities:
    StudentNames
     
Function Declaration:
    public static void main(String[] args)
     
Jobs To Be Done
    1. Create a list instance and add the given names.
    2. Replace the names to upper case.
    3. for each name in the list.
        3.1 check if the name start with 'A'
            3.1.1 Print the names.

Pseudo code:
class StudentName {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("krishnan", "abishek", "arun", "vignesh", "kiruthiga",
                "murugan", "adhithya", "balaji", "vicky", "priya");
        list.replaceAll(String::toUpperCase);
        for (String names : list) {
            if (names.startsWith("A")) {
                System.out.println(names);
            }
        }
    }
}
*/
package com.java.training.collections;

import java.util.List;
import java.util.Arrays;

public class StudentName {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("krishnan", "abishek", "arun", "vignesh", "kiruthiga",
                "murugan", "adhithya", "balaji", "vicky", "priya");
        System.out.println("list" + list);
        list.replaceAll(String::toUpperCase);
        System.out.println("Uppercase of the list is" + list);
        System.out.println("Name Starting with \"A\"");
        for (String names : list) {
            if (names.startsWith("A")) {
                System.out.println(names);
            }
        }
    }
}
