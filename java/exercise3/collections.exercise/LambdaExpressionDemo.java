/*
Requirement:
    Addition,Substraction,Multiplication and Division concepts are achieved using Lambda expression
    and functional interface

Entity:
    LambdaExample
    Arithmetic

Function declartion:
    int operation(int value1, int value2)

Jobs to be done:
    1. Create a functional interface containing operation method.
        1.1 Declare two parameter for the method.
    2. Define the method using lambda expression.
        2.1 Print the addition of two values.
        2.2 Print the subraction of two values.
        2.3 Print the multiplication of two values.
        2.4 Print the division of two values.

Pseudo code:
interface Arithmetic {
    int operation(int value1, int value2);
}

class LambdaExpressionDemo {
    public static void main(String[] args) {

        Arithmetic addition = (int value1, int value2) -> (value1 + value2);
        System.out.println(addition.operation(value1, value2));

        Arithmetic subtraction = (int value1, int value2) -> (value1 - value2);
        System.out.println("subtraction.operation(value1,value2));

        Arithmetic multiplication = (int value1, int value2) -> (value1 * value2);
        System.out.println(multiplication.operation(value1,value2));

        Arithmetic division = (int value1, int value2) -> (value1 / value2);
        System.out.println(division.operation(value1, value2));

    }
}

*/

package com.java.training.collections;

interface Arithmetic {
    int operation(int value1, int value2);
}


public class LambdaExpressionDemo {
    public static void main(String[] args) {

        Arithmetic addition = (int value1, int value2) -> (value1 + value2);
        System.out.println("Addition = " + addition.operation(5, 6));

        Arithmetic subtraction = (int value1, int value2) -> (value1 - value2);
        System.out.println("Subtraction = " + subtraction.operation(5, 3));

        Arithmetic multiplication = (int value1, int value2) -> (value1 * value2);
        System.out.println("Multiplication = " + multiplication.operation(4, 6));

        Arithmetic division = (int value1, int value2) -> (value1 / value2);
        System.out.println("Division = " + division.operation(12, 6));

    }
}
