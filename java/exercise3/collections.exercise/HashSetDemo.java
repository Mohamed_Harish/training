/*
Requirements:
    To demonstrate the basic add and traversal operation of linked hash set.
  
Entities:
    public class HashSetDemo
 
Function Declaration:
    public static void main(String[] args)
     
Jobs To Be Done:
    1. Create a hash set instance as set.
        1.1 Add elements to it.
    2. Remove an element from the set
        2.1 Print the set.
    3. Print the set using for each loop.
    
Pseudo code:
class HashSetDemo {

    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        set.add(elements);
        set.remove(element);
        System.out.println(set);

        for (int element : set) {
            System.out.println(element);
        }
    }

}

*/
package com.java.training.collections;

import java.util.HashSet;

public class HashSetDemo {

    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        set.add(115);
        set.add(220);
        set.add(223);
        set.add(222);
        set.add(226);
        System.out.println("Displaying elements in set " + set);
        set.remove(222);
        System.out.println("Printing the set after removing the element " + set);
        System.out.println("Displaying element using iterating method ");

        for (int element : set) {
            System.out.println(element + " ");
        }
    }

}
