/*
Requirement:
    8 districts are shown belowMadurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy
    to be converted to UPPERCASE.
Entity:
    Districts
Function Declaration:
    public static void main(String[] args)
Jobs to be done:
    1. Create a array instance as list and add the districts .
    2. Print the districts in upper case.
    
Pseudo code:
class District {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur",
                "Salem", "Erode", "Trichy");
        
        list.replaceAll(String::toUpperCase);
        System.out.println(list);   
    }
}

*/
package com.java.training.collections;

import java.util.Arrays;
import java.util.List;

public class District {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur",
                "Salem", "Erode", "Trichy");
        System.out.println("Modified list:");
        list.replaceAll(String::toUpperCase);
        System.out.println(list);   
    }

}
