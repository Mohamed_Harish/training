/*
Requirement:
    To code for sorting the vector in the descending order.

Entity:
    VectorDemo

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1. Create a Vector instance as vector.
        1.1 Add elements to it.
    2. Sort the elements in reversing order.
    3. Print the vector.

Pseudo code:
class VectorDemo {

    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<>();
        vector.add(elements);

        Collections.sort(vector, Collections.reverseOrder());
        System.out.println(vector);
    }

}
*/
package com.java.training.collections;

import java.util.Collections;
import java.util.Vector;

public class VectorDemo {

    public static void main(String[] args) {
        Vector<Integer> vector = new Vector<>();
        vector.add(115);
        vector.add(455);
        vector.add(121);
        vector.add(400);
        vector.add(768);
        vector.add(676);
        vector.add(556);
        System.out.println("Vector before sorting " + vector);

        Collections.sort(vector, Collections.reverseOrder());
        System.out.println("The vector after sorting in descending order is " + vector);
    }

}


