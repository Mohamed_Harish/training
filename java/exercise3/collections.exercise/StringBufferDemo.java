/*
Requirements:
    To demonstrate insertions and string buffer in tree set. 
     
Entities:
    public class StringBufferDemo 
      
Function Declaration:
    public static void main(String[] args)
      
Jobs To Done:
    1. Create an instance of a tree set.
        1.1 Add elements of string buffer to it.
    2. Print the elements.

Pseudo code:
class StringBufferDemo {

    public static void main(String[] args) {
        TreeSet<StringBuffer> treeSet = new TreeSet<>();
        treeSet.add(new StringBuffer(element));
        System.out.println(treeSet);
    }

}
*/

package com.java.training.collections;

import java.util.TreeSet;

public class StringBufferDemo {

    public static void main(String[] args) {
        TreeSet<StringBuffer> treeSet = new TreeSet<>();
        treeSet.add(new StringBuffer("Ab"));
        treeSet.add(new StringBuffer("Bc"));
        treeSet.add(new StringBuffer("Cd"));
        treeSet.add(new StringBuffer("De"));
        treeSet.add(new StringBuffer("Ef"));
        System.out.println(treeSet);
    }

}

/*
Explanation:
String class and all the Wrapper classes already implements Comparable interface but StringBuffer 
class doesn�t implements Comparable interface. Hence, we get a ClassCastException in the above 
example.
*/