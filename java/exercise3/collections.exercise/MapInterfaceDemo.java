/*
Requirement:
    To demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() )

Entity:
     public class MapInterfaceDemo

Function declaration:
     public static void main(String[] args) 

Jobs to be done:
     1. Create an instance of a tree map.
         1.1 Mapp the elements to it.
     2. Remove an element of the tree map.
         2.1 Print tree map.
     3. Check if a element is present in it.
     4. Replace the element in the tree map.
         4.1 Print the tree map.

Pseudo code:
class MapInterfaceDemo {
    
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(key,value);
        treeMap.remove(20);
        System.out.println(treeMap);
        System.out.println(treeMap.containsValue(value));
        
        treeMap.replace(key,value);
        System.out.println(treeMap);
    }
}
*/
package com.java.training.collections;

import java.util.TreeMap;

public class MapInterfaceDemo {
    
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(10, "Red");
        treeMap.put(20, "Green");
        treeMap.put(30, "Blue");
        treeMap.put(40, "Black");
        treeMap.put(50, "White");
        System.out.println("the treemap is " + treeMap);
        
        treeMap.remove(20);
        System.out.println("The map after removed value is: " + treeMap);
        System.out.println(treeMap.containsValue("White"));
        
        treeMap.replace(30, "Purple");
        System.out.println("The replaced map is: " + treeMap);
    }
}

