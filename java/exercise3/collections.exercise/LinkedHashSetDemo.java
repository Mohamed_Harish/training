/*
Requirement:
    Demonstrate linked hash set to array() method in java
      
Entity:
    public class LinkedHashSetDemo
  
Function:
    public static void main(String[] args)
      
Jobs to be Done:
    1. Create an instance of a linked hash set.
        1.1 Add elements to the set.
    2. Convert the linked hash set to array.
        2.1 Print the elements of the array using for each loop.

Pseudo code:
class LinkedHashSetDemo {

    public static void main(String[] args) {
        LinkedHashSet<String> set = new LinkedHashSet<>();
        set.add(elements);

        Object[] arr = set.toArray();

        for (Object name : arr) {
            System.out.println(name);
        }

    }

}

*/

package com.java.training.collections;

import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

    public static void main(String[] args) {
        LinkedHashSet<String> set = new LinkedHashSet<>();
        set.add("harish");
        set.add("ranjith");
        set.add("balaji");
        set.add("Sethu");
        set.add("prem");
        System.out.println("The Linked Hash Set " + set);

        Object[] arr = set.toArray();
        System.out.println("The array is");

        for (Object name : arr) {
            System.out.println(name + " ");
        }

    }

}
