/*
Requirement:
    To use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast() 
    methods to store and retrieve elements in ArrayDequeue.
    
Entity:
    ArrayDequeDemo
    
Method Signature:
    public static void main(String[] args)

Jobs to be Done:
    1. Create an object for ArrayDequeue as dequeue 
        1.1add the elements to it.
    2. Print the deque by adding a element first to it.
    3. Print the deque by adding a element last to it.
    4. Print the deque by removing first element from it.
    5. Print the deque by removing last element from it.
    6. Print the first peak of the deque.
    7. Print the last peak of the deque.
    8. Print the first poll element.
    9. Print the last poll element.
    
Pseudo code:
class ArrayDequeDemo {
    
    public static void main(String[] args) {
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        deque.add(elements);
        deque.addFirst(element);
        System.out.println(deque);
        
        deque.addLast(element);
        System.out.println(deque);
        
        deque.removeFirst();
        System.out.println(deque);
        
        deque.removeLast();
        System.out.println(deque);
        
        System.out.println(deque.peekFirst());
        System.out.println(deque.peekLast());
        
        System.out.println(deque.pollFirst());
        System.out.println(deque.pollLast());

    }

}

*/
package com.java.training.collections;

import java.util.ArrayDeque;

public class ArrayDequeDemo {
    
    public static void main(String[] args) {
        ArrayDeque<Integer> deque = new ArrayDeque<>();
        deque.add(1);
        deque.add(2);
        deque.add(3);
        deque.add(4);
        deque.add(5);
        deque.add(6);
        deque.add(7);
        deque.add(8);
        deque.add(9);
        deque.add(10);
        System.out.println("The ArrayDeque " + deque);
        
        deque.addFirst(0);
        System.out.println("After adding the first element" + deque);
        
        deque.addLast(11);
        System.out.println("After adding the last element" + deque);
        
        deque.removeFirst();
        System.out.println("After removing the first element" + deque);
        
        deque.removeLast();
        System.out.println("After removing the last element" + deque);
        
        System.out.println("The first peek element " + deque.peekFirst());
        System.out.println("The last  peek element " + deque.peekLast());
        
        System.out.println("The first poll element " + deque.pollFirst());
        System.out.println("The last poll element " + deque.pollLast());

    }

}
