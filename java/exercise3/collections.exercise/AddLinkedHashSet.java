/*
Requirements:
    To demonstrate the basic add and traversal operation of linked hash set.
  
Entities:
    AddHashSet 
 
Function Declaration:
    public static void main(String[] args)
     
Jobs To Be Done:
    1. Create a linked hash set instance as set.
        1.1 Add elements to the set.
    2. Create an iterator for the set.
        2.1 Print the set elements using the iterator.
        
Pseudo code:
class AddLinkedHashSet {

    public static void main(String[] args) {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        set.add(elements);
        
        Iterator<Integer> iterator = set.iterator();

        while (iterator.hasNext()) {
            System.out.print(iterator.next());
        }
    }
}

*/
package com.java.training.collections;

import java.util.Iterator;
import java.util.LinkedHashSet;

public class AddLinkedHashSet {

    public static void main(String[] args) {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        System.out.println("Set elements are " + set);
        System.out.println("Set elements are using iterator");

        Iterator<Integer> iterator = set.iterator();

        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }

        System.out.println();
    }
}
