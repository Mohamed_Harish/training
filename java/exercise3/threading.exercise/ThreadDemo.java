/*
Requirement:
    To write a program of performing two tasks by two threads that implements Runnable interface.

Entity:
    ThreadDemo
    
Function Declaration:
    Public static void main(String[] args)
    
Jobs to be done:
    1. Declare a two task that is to be performed concurrently.
    2. Run the tasks.
*/
package com.java.training.multithreading;

public class ThreadDemo implements Runnable {

    public void run() {
        System.out.println("My task one");
    }


    public static void main(String[] args) {
        Runnable runnable = new ThreadDemo();
        Thread thread1 = new Thread(runnable);
        
        Thread thread2 = new Thread(runnable) {
            public void run() {
                System.out.println("My task two");
            }
        };
        thread1.start();
        thread2.start();

    }
}
