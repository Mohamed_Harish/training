/*
Requirement:
    To Explain about contains, retainAll() and subString

Entity:
    public class ListMethodDemo

function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an arraylist instance as integer
        1.1 Add elements to the list.
        1.2 Check whether a value is present or not.
    2. Create an array list instance as oddInteger.
        2.1 Add elements to it.
        2.2 Retain all the elements of integer.
        
    3. Create an array list instance as string.
        3.1 Add elements to it.
    4. Create an another array list as subArray
        4.1 Add elements to it using slicing of string.

Pseudo code:
class ListMethodDemo {

    public static void main(String[] args) {
        List<Integer> integer = new ArrayList<>();
        integer.add(elements);

        // contains is used to check whether the value is present
        if (integer.contains(element) == true) {
            System.out.println("the value is present");
        } else {
            System.out.println("The value is not present");
        }

        // retainAll() method used to retain all the matching elements
        List<Integer> oddInteger = new ArrayList<>();
        oddInteger.add(elements);
        integer.retainAll(oddInteger);
        System.out.println(integer);

        // Sublist is used to produce the desired values from the given indexes.
        List<String> string = new ArrayList<>();
        string.add(elements);
        System.out.println(string);

        // now create another list
        List<String> subString = new ArrayList<>();
        subString = string.subList(index1,index2);
        System.out.println(subString);
    }
}

*/
package com.java.training.list;

import java.util.List;
import java.util.ArrayList;

public class ListMethodDemo {

    public static void main(String[] args) {
        List<Integer> integer = new ArrayList<>();
        integer.add(10);
        integer.add(20);
        integer.add(30);

        // contains is used to check whether the value is present
        if (integer.contains(25) == true) {
            System.out.println("the value is present");
        } else {
            System.out.println("The value is not present");
        }

        // retainAll() method used to retain all the matching elements
        List<Integer> oddInteger = new ArrayList<>();
        oddInteger.add(30);
        oddInteger.add(20);
        oddInteger.add(55);
        integer.retainAll(oddInteger);
        System.out.println(" the result of retainAll method is" + " " + integer);

        // Sublist is used to produce the desired values from the given indexes.
        List<String> string = new ArrayList<>();
        string.add("For");
        string.add("produce");
        string.add("exact");
        string.add("copy");
        string.add("material");
        System.out.println(" the List is" + string);

        // now create another list
        List<String> subString = new ArrayList<>();
        subString = string.subList(2, 4);
        System.out.println("The sublist is " + " " + subString);
    }
}
