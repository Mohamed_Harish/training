/*
Requirement:
    To create a list and to add 10 elements 
    1. create another list and to use add all method
    2. Use indexOf() and lastIndexOf() methods
    3. To print the elements using for loops and iterator and stream
    4. To convert the list to string and array.

Entity:
    public class ListDemo
    
Function Declaration:
    public static void main(String[] args)
    indexOf(),lastIndex(), list.Stream(), hasNext(), next().

Jobs to be done:
    1. Create an arraylist instance and add 10 elements to it.
    2. Create a another list and perform addAll, indexOf(), lastIndexOf() methods.
    3. Convert the list to string and array.
    4. Print the list elements using forloop, iterator and stream.
    
Pseudo code:
class ListDemo {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        // Add the elements to the list.
        list.add(elements);
        System.out.println(list);

        List<String> list1 = new ArrayList<>();
        list1.add(elements);
        System.out.println(list1);

        // Now add all elements in list1 to list
        list.addAll(list1);

        // Print the index of first element
        System.out.println(list.indexOf(first element));

        // print the index of last element
        System.out.println(list.lastIndexOf(last element));

        // print the elements using enhanced for loop
        for (String value : list) {
            System.out.print(value + " ");
        }

        // Creating an array for a list
        String[] colorArray = new String[list.size()];
        colorArray = list.toArray(colorArray);
        System.out.println(Arrays.toString(colorArray));

        // Converting List into an Set
        Set<String> alphaSet = new HashSet<>(list);
        System.out.println("The Set Values are");
        for (String value1 : alphaSet) {
            System.out.print(value1 + " ");
        }

        // print the elements using for loop
        for (int integer = 0; integer < list.size(); integer++) {
            System.out.println(list.get(integer));
        }

        // Using Iterator
        Iterator<String> iteratorList = list.iterator();
        while (iteratorList.hasNext()) {
            System.out.println(iteratorList.next());
        }

        // Using Stream
        list.stream().forEach((element) -> System.out.print(element));
    }
}

*/
package com.java.training.list;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

public class ListDemo {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        // Add the elements to the list.
        list.add("Black");
        list.add("White");
        list.add("Orange");
        list.add("Scorpian");
        list.add("Velarian");
        list.add("Indica");
        list.add("Vilot");
        list.add("Brown");
        list.add("Purple");
        list.add("Yellow");
        // Print the list.
        System.out.println("The list is" + " " + list);

        List<String> list1 = new ArrayList<>();
        list1.add("Red");
        list1.add("Green");
        list1.add("Blue");
        System.out.println("The Second list is" + " " + list1);

        // Now add all elements in list1 to list
        list.addAll(list1);

        // print the list
        System.out.println("The Modified list is " + " " + list);

        // Print the index of first element
        System.out.println("the first index is" + " " + list.indexOf("Orange"));

        // print the index of last element
        System.out.println("The last index is" + " " + list.lastIndexOf("purple"));

        // print the elements using enhanced for loop
        System.out.println("Using enhanced for loop");
        for (String value : list) {
            System.out.print(value + " ");
        }
        System.out.println();

        // Creating an array for a list
        String[] colorArray = new String[list.size()];
        colorArray = list.toArray(colorArray);
        System.out.println("the list in array " + " " + Arrays.toString(colorArray));

        // Converting List into an Set
        Set<String> alphaSet = new HashSet<>(list);
        System.out.println("The Set Values are");
        for (String value1 : alphaSet) {
            System.out.print(value1 + " ");
        }
        System.out.println();

        // print the elements using for loop
        System.out.println("Using for loop");
        for (int integer = 0; integer < list.size(); integer++) {
            System.out.println("The elements are" + " " + list.get(integer));
        }

        // Using Iterator
        System.out.println("Using Iterator");
        Iterator<String> iteratorList = list.iterator();
        while (iteratorList.hasNext()) {
            System.out.println("the elements are" + " " + iteratorList.next());
        }

        // Using Stream
        System.out.println("Using stream");
        list.stream().forEach((element) -> System.out.print(element));
    }
}
