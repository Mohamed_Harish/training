/*
Requirement:
    To create a table and to add elements and to use addAll() and removeAll().
    
Entity:
    SetDemo
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create an instance of a hashset.
    2. Add elements to that set.
    3. Create an another instance of a hashset and add elements to it.
        3.1 Add all elements of string to string1.
        3.2 Print the elements of string2 using iterator
        3.3 Print the elements using for each loop.
        3.4 Remove all elements from the set.
        
Pseudo code:
class SetDemo {

    public static void main(String[] args) {
        Set<String> string = new HashSet<>();
        // add elements
        string.add(elements);

        Set<String> string2 = new HashSet<>();
        string2.add(elements);
        System.out.println("the second set is" + " " + string2);

        string2.addAll(string);
        System.out.println("the modified set is " + " " + string2);

        // Using iterator
        Iterator<String> iterator = string.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.print(element + " ");
        }

        // using for each loop
        for (String value : string) {
            System.out.print(value + " ");
        }
        System.out.println("the removed set is" + " " + string.removeAll(string2));
    }
}

*/
package com.java.training.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {

    public static void main(String[] args) {
        Set<String> string = new HashSet<>();
        string.add("Cricket");
        string.add("Volleyball");
        string.add("Football");
        string.add("Kabadi");
        string.add("Hockey");
        string.add("Ice Skating");
        string.add("BasketBall");
        string.add("Throwball");
        string.add("Rugby");
        string.add("Tennis");
        System.out.println("The Set1 is " + " " + string);

        Set<String> string2 = new HashSet<>();
        string2.add("Dhoni");
        string2.add("Prabakaran");
        System.out.println("the second set is" + " " + string2);

        string2.addAll(string);
        System.out.println("the modified set is " + " " + string2);

        // Using iterator
        System.out.println("Using Iterator");
        Iterator<String> iterator = string.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.print(element + " ");
        }
        System.out.println();

        // using for each loop
        System.out.println("Using for each loop");
        for (String value : string) {
            System.out.print(value + " ");
        }
        System.out.println();

        // to remove all the elements
        System.out.println("the removed set is" + " " + string.removeAll(string2));
    }
}
