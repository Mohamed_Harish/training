/*
Requirement:
    To describe about the contains() method and isEmpty() method.

Entity:
    SetExample
    
Function Declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1. Create an instance of a hashset setA.
    2. Add elements to the setA.
    3. Perform contains and isempty method using that setA.
    
Pseudo code:
class SetMethod {

    public static void main(String[] args) {
        Set<String> setA = new HashSet<>();
        // add elements
        setA.add(elements);
        System.out.println("Does the Set setA contains Writer" + " " + setA.contains(element));
        System.out.println("IS the Set is empty" + " " + setA.isEmpty());
    }
}
*/
package com.java.training.set;

import java.util.Set;
import java.util.HashSet;

public class SetMethod {

    public static void main(String[] args) {
        Set<String> setA = new HashSet<>();
        setA.add("Hero");
        setA.add("Heroien");
        setA.add("Director");
        setA.add("Editor");
        setA.add("Music Director");
        System.out.println("Does the Set setA contains Writer" + " " + setA.contains("Writer"));
        System.out.println("IS the Set is empty" + " " + setA.isEmpty());
    }
}
