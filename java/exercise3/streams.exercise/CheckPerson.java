/*
Requirement:
    Consider the following Person:
        new Person(
           "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
    - Check if the above person is in the roster list obtained from Person class.
  
Entity:
    CheckPerson
  
Function Declaration:
    public static void main(String[] args)
  
Jobs To Be Done:
    1. Declare an boolean variable present and assign it to false.
    2. Create a list of type person and assign createroster method to it.
    3. Create an object called newperson.
    4. Check if it matches the person in person.
       4.1 If it matches, print true.
       4.2 If it doesn't print false.
       
Pseudo code:
class CheckPerson {

    public static boolean present = false;

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person newPerson = new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> {
            if ((person.getName().equals(newPerson.getName()))
                    && (person.getBirthday().equals(newPerson.getBirthday()))
                    && (person.getEmailAddress().equals(newPerson.getEmailAddress()))
                    && (person.getGender().equals(newPerson.getGender()))) {
                present = true;
            }
        });
        if (present == true) {
            System.out.println("Person is present");
        } else {
            System.out.println("Person is not present");
        }

    }
}

*/
package com.java.training.streams;

import java.time.chrono.IsoChronology;
import java.util.List;
import java.util.stream.Stream;

public class CheckPerson {

    public static boolean present = false;

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person newPerson = new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> {
            if ((person.getName().equals(newPerson.getName()))
                    && (person.getBirthday().equals(newPerson.getBirthday()))
                    && (person.getEmailAddress().equals(newPerson.getEmailAddress()))
                    && (person.getGender().equals(newPerson.getGender()))) {
                present = true;
            }
        });
        if (present == true) {
            System.out.println("Person is present");
        } else {
            System.out.println("Person is not present");
        }

    }
}
