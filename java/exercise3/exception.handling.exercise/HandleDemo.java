/*
Requirement:
    To handle the given program.
       public class Exception {  
           public static void main(String[] args) {
               int arr[] ={1,2,3,4,5};
               System.out.println(arr[7]);
           }
       }
Entity:
    public class HandleDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an integer array containing 6 elements.
        1.1 Print the 7th element in the array.
    2. Handle the exception and produce the user understandable message.
        2.1 Handle the exception by array index out of bound.
        2.2 Handle the exception by Exception .
    3. Print the message. 
Pseudo code:
class HandleDemo {

    public static void main(String[] args) {
        try {
            int[] array = {1, 2, 3, 4, 5, 6};
            System.out.println(array[7]);
        } catch (ArrayIndexOutOfBoundsException exception1) {
            System.out.println(message);
        } catch (Exception exception2) {
            System.out.println(message);
        }
    }
}
*/
package com.java.training.exceptionhandling;

public class HandleDemo {

    public static void main(String[] args) {
        try {
            int[] array = {1, 2, 3, 4, 5, 6};
            System.out.println(array[7]);
        } catch (ArrayIndexOutOfBoundsException exception1) {
            System.out.println("Not in the index level");
        } catch (Exception exception2) {
            System.out.println("The exception is produced");
        }
    }
}
