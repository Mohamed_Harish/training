/*
Requirement:
    To write a program using try and catch.

Entity:
    public class TryCatchDemo.

Function Declaration:
    public static void main(String[] args)
    public void writeList()

Jobs to be done:
    1. Create a integer array of size 10.
    2. Declare a method writeList.
        2.1 Store a value in 12th index.
    3. Print the exception message using try and catch block.
Pseudo code:
class TryCatchDemo {

    public int[] arrayOfNumbers = new int[10];

    void writeList() {
        try {
            arrayOfNumbers[12] = value;
        } catch (NumberFormatException exception1) {
            System.out.println(message);
        } catch (IndexOutOfBoundsException exception2) {
            System.out.println(message);
        }
    }

    public static void main(String[] args) {
        TryCatchDemo list = new TryCatchDemo();
        list.writeList();
    }
}

*/
package com.java.training.exceptionhandling;

public class TryCatchDemo {

    public int[] arrayOfNumbers = new int[10];

    public void writeList() {
        try {
            arrayOfNumbers[12] = 30;
        } catch (NumberFormatException exception1) {
            System.out.println("Enter a valid number");
        } catch (IndexOutOfBoundsException exception2) {
            System.out.println("Check the size of the array");
        }
    }

    public static void main(String[] args) {
        TryCatchDemo list = new TryCatchDemo();
        list.writeList();
    }
}
