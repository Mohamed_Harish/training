/*
Requirement:
    To demonstrate the difference between Checked and Unchecked Exception.

Entity:
    public class CheckedAndUncheckedDemo

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an integer array containing 6 elements.
        1.1 Print the 7th element of the array.
    2. Open a file that is not Present.
        2.1 Read the text in that file.
        2.2 Print the text.
    3. Analyze the exception produced.
    
Pseudo code:
class CheckedAndUncheckedDemo {

    public static void main(String[] args) {
        // Example for an unchecked Exception.
        // Exception does not be produced during run time.
        
        int[] array = {1, 2, 3, 4, 5, 6};
        System.out.println("The 7th element is " + "  " + array[6]);
        
        // Checked Exception - Throws compile time exception.
        //It throws FileNotFoundException if the file is not found
        
        file = new FileInputStream("C:/myfile.txt");
        while ((text = file.read()) != -1) {
            System.out.println((char) text);
        }

    }
}

*/
package com.java.training.exceptionhandling;

//import java.io.FileInputStream;

public class CheckedAndUncheckedDemo {

    public static void main(String[] args) {
        // Example for an unchecked Exception.
        // Exception does not be produced during run time.
        int[] array = {1, 2, 3, 4, 5, 6};
        System.out.println("The 7th element is " + "  " + array[6]);
        
        // Checked Exception - Throws compile time exception.
        //It throws FileNotFoundException if the file is not found
        /*FileInputStream file = null;
        file = new FileInputStream("C:/myfile.txt");
        int text;
        while ((text = file.read()) != -1) {
            System.out.println((char) text);
        }

        file.close(); // Throws IO exception.*/
    }
}
