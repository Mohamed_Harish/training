/*
Requirement:
    To demonstrate whether a multiple try block can be used for single catch.

Entity:
    public class MultipleTryBlockDemo 

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create two integers as number1 and number2.
    2. Assign values in a try block.
        2.1 Perform  division operation in another try block.
        2.2 Print the number3 as output.
    3. Declare exception in catch block.
    4. Analyze whether multiple try block can be used for single catch.

Pesudo code:
class MultipleTryBlockDemo {

    public static void main(String[] args) {
        int number1;
        int number2;
       // Error occurs try needs catch or finally to complete
       // But we can use nested try block, it is valid
        try {
            number1 = 10;
            number2 = 5;
        }
        
        try {
            int number3 = number1 / number2;
            System.out.println("number3");
        } catch (Exception e) {
            System.out.println(message);
        }
        
    }
}
*/
package com.java.training.exceptionhandling;

public class MultipleTryBlockDemo {

    public static void main(String[] args) {
        int number1;
        int number2;
       // Error occurs try needs catch or finally to complete
       // But we can use nested try block, it is valid
        /*try {
            number1 = 10;
            number2 = 5;
        }
        
        try {
            int number3 = number1 / number2;
            System.out.println("number3");
        } catch (Exception e) {
            System.out.println("Give the arguments");
        }*/
        
    }
}
