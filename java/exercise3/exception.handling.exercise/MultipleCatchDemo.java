/*
Requirement:
    To Demonstrate catching multiple exception.

Entity:
    public class MultipleCatchDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create an integer array of size 5.
        1.1 Perform a division operation.
        1.2 Store the value in 6th place.
    2. Print the exception message using multiple catch block.

Pseudo code:
class MultipleCatchDemo {

    public static void main(String[] args) {
        try {
            int[] array = new int[5];
            array[5] = 30 / 5;
            System.out.println(array[5]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println(message1);
        } catch (ArithmeticException e) {
            System.out.println(message2);
        } catch (Exception e) {
            System.out.println(message3);
        }

    }
}  
*/
package com.java.training.exceptionhandling;

public class MultipleCatchDemo {

    public static void main(String[] args) {
        try {
            int[] array = new int[5];
            array[5] = 30 / 5;
            System.out.println(array[5]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array Index Out Of Bounds Exception occurs");
        } catch (ArithmeticException e) {
            System.out.println("Arithmetic Exception occurs");
        } catch (Exception e) {
            System.out.println("Parent Exception occurs");
        }

        System.out.println("rest of the code");
    }
}
