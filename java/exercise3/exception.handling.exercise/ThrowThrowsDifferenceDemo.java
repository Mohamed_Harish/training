/*
Requirement:
    To demonstrate the difference between throw and throws.

Entity:
    public class ThrowThrowsDiffernceDemo.

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1. Create a method as checkAge
        1.1 Pass the age as parameter.
        1.2 Check if the age greater than 18
            1.2.1 throw arithmatic exception message.
    2. Create a method division.
        2.1 Declare arithmetic exception for that method.
        2.2 Divide the two numbers.
        2.3 Print the quotient.
    3. Verify the difference between throw and throws key word.
    
Pseudo code:
class ThrowThrowsDifferenceDemo {

    public void checkAge(int age) {

        // Throw keyword is used to throw an exception explicitly.
        if (age < 18) {
            throw new ArithmeticException(message);
        } else {
            System.out.println(message);
        }
    }

    // Throws is used to declare an exception, which means it works similar to the try-catch.
    public void division(int number1, int number2) throws ArithmeticException {
        int quotient = number1 / number2;
        System.out.println(quotient);
    }

    public static void main(String args[]) {
        ThrowThrowsDifferenceDemo object = new ThrowThrowsDifferenceDemo();
        object.checkAge(age);

        ThrowThrowsDifferenceDemo object1 = new ThrowThrowsDifferenceDemo();
        try {
            object1.division(number1, number2);
        } catch (ArithmeticException exception) {
            System.out.println(message);
        }
    }
}
    
*/
package com.java.training.exceptionhandling;

public class ThrowThrowsDifferenceDemo {

    public void checkAge(int age) {

        // Throw keyword is used to throw an exception explicitly.
        if (age < 18) {
            throw new ArithmeticException("Not Eligible for voting");
        } else {
            System.out.println("Eligible for voting");
        }
    }

    // Throws is used to declare an exception, which means it works similar to the try-catch.
    public void division(int number1, int number2) throws ArithmeticException {
        int quotient = number1 / number2;
        System.out.println(quotient);
    }

    public static void main(String args[]) {
        ThrowThrowsDifferenceDemo object = new ThrowThrowsDifferenceDemo();
        object.checkAge(13);
        System.out.println("End Of Program");

        ThrowThrowsDifferenceDemo object1 = new ThrowThrowsDifferenceDemo();
        try {
            object1.division(15, 0);
        } catch (ArithmeticException exception) {
            System.out.println("You shouldn't divide number by zero");
        }
    }
}
