/* 
Requirement: 
    To check whether we can write only try block without catch and finally blocks?why?

Entity:
    public class TryCheck

Function declaration: 
    public static void main(String[] args)

Jobs to be done:  
    1. Print the division two numbers in a try block.
    2. Analyze without using catch block.
    
Pseudo code:
class TryCheck {

    public static void main(String[] args) {

       try{
           System.out.println(2/0);
       }
         
    }
}
 
Solution:
   In a a program try block should be accompanied by either catch block or finally block or both.
A program without catch or finally block will throw syntax error as the exception will not get handled.
*/
package com.java.training.exceptionhandling;

public class TryCheck {

    public static void main(String[] args) {

       /* try{
           System.out.println(2/0);
        }*/
        //Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
        //Syntax error, insert "Finally" to complete BlockStatements

    }
}
