/*
Requirement:
    To print the type of the result value of following expressions
    - 100 / 24
    - 100.10 / 10
    - 'Z' / 2
    - 10.5 / 0.5
    - 12.4 % 5.5
    - 100 % 56

Entity:
    DataTypeDemo
    
Funtion declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1) Create object variable and assign the given expression.
    2) print the type of the result value of the object variable.
*/

//Program:
package com.java.training.datatype;

public class DataTypeDemo {

    public static void main(String[] args) {
        Object expression1 = 100 / 24;
        System.out.println(expression1.getClass().getName());
        Object expression2 = 100.10 / 10;
        System.out.println(expression2.getClass().getName());
        Object expression3 = 'Z' / 2;
        System.out.println(expression3.getClass().getName());
        Object expression4 = 10.5 / 0.5;
        System.out.println(expression4.getClass().getName());
        Object expression5 = 12.4 % 5.5;
        System.out.println(expression5.getClass().getName());
        Object expression6 = 100 / 56;
        System.out.println(expression6.getClass().getName());
    }
}
