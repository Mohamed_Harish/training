/*
Requirement:
    To print the Class names of the primitive datatype.

Entity:
    PrimitiveClassName

Function declaration:
    public static void main(String[] args)
    getName()

Jobs to be done:
    1.Print the respective classname of primitive datatype.
*/

//Program:
package com.java.training.datatype;

public class PrimitiveClassName {

    public static void main(String[] args) {
        String className1 = int.class.getName();
        System.out.println("Class Name of Int : " + className1);

        String className2 = char.class.getName();
        System.out.println("Class Name of Char : " + className2);

        String className3 = double.class.getName();
        System.out.println("Class Name of Double : " + className3);

        String className4 = float.class.getName();
        System.out.println("Class Name of Float : " + className4);
    }
}
