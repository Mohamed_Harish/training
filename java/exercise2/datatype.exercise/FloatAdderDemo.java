/*
Requirement:
    To create a program that is similar to the previous one but instead of reading integer
    arguments,it reads floating-point arguments.It displays the sum of the arguments, using
    exactly two digits to the right of the decimal point.

Entity:
    FloatAdderDemo

Function declaration:
    public static void main(String[] args)
    format()

Jobs to be done:
    1)Create a method with vararg of integer type as parameter.
        1.1)check whether the parameter length is equal to 1.
            1.1.1)if true, then print "Add more numbers"
            1.1.2)if false, then 
                1.1.2.1)initialize a float variable sum with value as 0.
                1.1.2.2)for each number in the vararg parameter.
                1.1.2.3)add each number to the sum.
                1.1.2.4)Print the value of sum with two decimal values.
    2)Invoke the method with n float values as parameter.
*/

//Program:
package com.java.training.datatype;

import java.text.DecimalFormat;

public class FloatAdderDemo {

    public static void main(String[] args) {
        float number1 = Float.parseFloat(args[0]);
        if (args.length < 2) {
            System.out.println("Enter 2 or more value");
        } else {
            double sum = 0.0;
            for (int value = 0; value < args.length; value++) {
                sum += Double.valueOf(args[value]).doubleValue();
            }

            DecimalFormat myFormatter = new DecimalFormat("###,###.##");
            String total = myFormatter.format(sum);
            System.out.println(total);
        }
    }
}
