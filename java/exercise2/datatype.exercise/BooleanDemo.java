/*
Requirement:
    To invert the value of a boolean,using an operator.

Entity:
    BooleanDemo

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1) Assign a boolean expression to the boolean variable.
    2) print the inverted value of a boolean variable.
*/

//Program:
package com.java.training.datatype;

public class BooleanDemo {

    public static void main(String[] args) {
        boolean word = !("hello" == "hello"); // logical complement operator(!)is used to invert
        System.out.println("Inverted boolean : " + word);
    }
}
