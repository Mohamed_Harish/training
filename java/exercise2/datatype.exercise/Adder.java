/*
Requirement:
    To create a program that reads an unspecified number of integer arguments from
    the command line and adds them together.

Entity:
    Adder

Function declaration:
    printAdd(int... numbers)
    public static void main(String[] args)
    
Jobs to be done:
    1) Assign the given values as array and store it to array variable.
    2) Call the method and pass the array.
    3) Check if array length is "1" 
           3.1) if yes,prints Add more new numbers.
           3.2) otherwise, initialize the sum is equal to zero.
           3.3) Add the each element in the array and store it to the sum.
           3.4) print the array.
*/
//Program:
package com.java.training.datatype;

public class Adder {

    public void printAdd(int... numbers) {
        if (numbers.length == 1) {
            System.out.println("Add more numbers");
        } else {
            int sum = 0;
            for (int number : numbers) {
                sum += number;
            }
            System.out.println(sum);
        }
    }

    public static void main(String[] args) {
        Adder adder = new Adder();
        adder.printAdd(2, 3, 4);
    }
}
