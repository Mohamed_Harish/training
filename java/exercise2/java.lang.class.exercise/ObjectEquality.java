/*
Requirement:
    To demonstrate object equality using Object.equals() vs ==,using String objects.

Entity:
    ObjectEquality

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1)Two string value is assigned to two string variables.
    2)Check two string values with "==" operator.
        2.1)Whether both the string values are equal and has same address, print true.
        2.2)Otherwise, print false.
    3)Check two string values with equals().
        3.1)Whether both the string values are equal, print true.
        3.2)otherwise, print false.
*/
//Program:
package com.java.training.langclass;

public class ObjectEquality {

    public static void main(String[] args) {
        String word1 = new String("java");
        String word2 = new String("java");
        System.out.println(word1 == word2);
        System.out.println(word1.equals(word2));
    }
}
