package com.java.training.abstractclass;

public class Circle extends Shape {

    public double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public void printArea() {
        System.out.println("area of the circle:" + 3.14 * radius * radius);
    }

    public void printPerimeter() {
        System.out.println("perimeter of the circle:" + 2 * 3.14 * radius);
    }
}
