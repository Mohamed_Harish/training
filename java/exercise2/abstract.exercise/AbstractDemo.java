package com.java.training.abstractclass;

import java.util.Scanner;

public class AbstractDemo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double side = scanner.nextDouble();
        double radius = scanner.nextDouble();
        Square square = new Square(side);
        square.printArea();
        square.printPerimeter();
        Circle circle = new Circle(radius);
        circle.printArea();
        circle.printPerimeter();
        scanner.close();
    }
}
