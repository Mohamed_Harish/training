package com.java.training.abstractclass;

public class Square extends Shape {

    public double side;

    public Square(double side) {
        this.side = side;
    }

    public void printArea() {
        System.out.println("area of the square:" + side * side);
    }

    public void printPerimeter() {
        System.out.println("perimeter of the square:" + 4 * side);
    }
}
