/*
Requirement:
    To demonstrate the abstract class using the class shape that has two methods to calculate the
    area and perimeter of two classes named the Square and the Circle extended from the class Shape.

Entity:
    abstract class Shape.
    Class Square.
    Class Circle.

Function declaration:
    public void printArea();
    public void printPerimeter();
    public static void main(String[] args).

Jobs to be done:
    1)Declare the abstract method printArea and
    printPerimeter.
    2) Declare a public variable.
        2.1)Declare a constructor with a parameter.
        2.2)Assign the parameter value to the variable.
    3)Declare a method.
        3.1)Print the area of the square.
    4)Declare a method.
        4.1)Print the perimeter of square.
    5)Declare the variable radius.
    6)Declare a constructor with one parameter as type double.
        6.1)Assign the parameter value to radius.
    7)Declare a method.
        7.1)Print the area of circle.
    8)Declare a method.
        8.1)Print the perimeter of the circle.
*/

//Program:
package com.java.training.abstractclass;

public abstract class Shape {

    public abstract void printArea();

    public abstract void printPerimeter();
}
