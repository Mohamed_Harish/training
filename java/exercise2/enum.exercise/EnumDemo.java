/*
Requirement:
    To compare the enum values using equals method and == operator.

Entity:
    public class EnumDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1)Create a variable and assign null.
    2)Create a enum type Mentor and store the values.
    3)Create a enum type Student and store the values.
    4)Compare using "==" operator.
        4.1)Check if the two Strings point to the same address,then print true.
        4.2)otherwise print false.
    5)Compare using equals.
        5.1)check if the two strings are equal,then print true.
        5.2)otherwise print false.
    6)Add a new element to the enum Student.
    7)Compare using "==" operator.
        7.1)Check if the two Strings point to the same address,then print true.
        7.2)otherwise print false.
    8)Compare using equals.
        8.1)check if the two strings are equal,then print true.
        8.2)otherwise print false.
*/
//Program:
package com.java.training.enumtype;

public class EnumDemo {

    public enum Direction {
        east, west, north, south
    }

    public static void main(String[] args) {
        Direction direction = Direction.east;
        Direction directionNew = null;
        System.out.println(direction == directionNew); // false
        System.out.println(directionNew == Direction.east); // false
        System.out.println(direction.equals(directionNew)); // false
        System.out.println(directionNew.equals(direction)); // shows null pointer exception
    }
}
