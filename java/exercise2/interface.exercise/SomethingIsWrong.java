/*
Requirement:
    To identify what is wrong in the given interface program.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }

Entity:
    SomethingIsWrong.

Function declaration:
    void aMethod(int aValue).

Jobs to be done:
    1.Consider the given program.
    2.Correct the program by identifying the error.

Solution:
    The methods of interface should not be defined.
*/
//Program
package com.java.training.interfaces;

public interface SomethingIsWrong {
    void aMethod(int aValue);
}
