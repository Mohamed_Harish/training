package com.java.training.iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class IterableDemo implements Iterable<String> {

    public ArrayList<String> employeeList;

    public IterableDemo() {
        employeeList = new ArrayList<String>();
    }

    public void addEmployee(String employee) {
        employeeList.add(employee);
    }

    public Iterator<String> iterator() {
        return employeeList.iterator();
    }

    public void checkMethod() {
        employeeList.forEach((member) -> {
            System.out.println(member);
        });
    }

    public static void main(String[] args) {
        IterableDemo employees = new IterableDemo();
        // ArrayList<String> employeeList = new ArrayList<>();
        employees.addEmployee("Kiran");
        employees.addEmployee("Pavi");
        employees.addEmployee("Harish");
        employees.addEmployee("Dave");
        employees.checkMethod();
        System.out.println("--------");
        for (String name : employees) {
            System.out.println(name);
        }
    }
}
