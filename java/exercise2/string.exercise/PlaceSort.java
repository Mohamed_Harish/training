/*
Reference:
    To sort and print the strings alphabetically ignoring case. Also convert and print even indexed
    Strings into uppercase{ "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }
    
Entity:
    PlaceSort
    
Function declaration:
    public static void main(String[] args)
    
Jobs to be done:
    1) To print the given city names in sorted order, the city names are assigned to the String list.
    2) The list is sorted and printed.
    3)For each city name, 
        3.1)Check if the city name is present in even index.
            3.1.1)If yes, convert that particular city into UpperCase.
*/
//Program:
package com.java.training.string;

import java.util.Arrays;

public class PlaceSort {

    public static void main(String[] args) {
        String[] place = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem"};
        System.out.println("Given Array :" + Arrays.toString(place));
        Arrays.sort(place, String.CASE_INSENSITIVE_ORDER);
        for (int word = 0; word < place.length; word++) {
            if (word % 2 == 0) {
                place[word] = place[word].toUpperCase();
            }
        }
        System.out.println("Sorted Array :" + Arrays.toString(place));
    }
}
