/*
Requirement:
    To write a program that computes my initials of my full name and displays them.

Entity:
    MyInitials.

Function declaration:
    public static void main(String[] args)

Jobs to be done:
     1) A name is assigned to the string variable.
    2) Each String value is splited and assigned to the list.
    3) For Each values of the name 
        3.1)Check Whether the letter at beginning and after space are lower case.
            3.1.1)If true, print the upperCase of the letter
            3.1.2) Otherwise print the character.
*/

//Program:
package com.java.training.string;

import java.util.Scanner;

public class MyInitial {

    public static void main(String[] args) {
        String name;
        Scanner scanner = new Scanner(System.in);
        name = scanner.nextLine();
        name = " " + name;
        for (int letter = 0; letter < name.length(); letter++) {
            if (name.charAt(letter) == ' ' && Character.isUpperCase(name.charAt(letter + 1))) {
                System.out.print(name.charAt(letter + 1) + " ");
            }
        }
        scanner.close();
    }
}
