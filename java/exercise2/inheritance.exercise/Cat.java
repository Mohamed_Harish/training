/*
Entity:
    Cat

Function declaration:
    public void sound()

Jobs to be done:
    1.Declare the class Cat.
    2.Call the method sound().
    3.Print the the statement as given.
*/
package com.java.training.inheritance;

public class Cat extends Animal {

    public void sound() {
        System.out.println("Cat meows");
    }
}
