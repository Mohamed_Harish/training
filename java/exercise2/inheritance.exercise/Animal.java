/*
Requirement:
    To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake 
    class of objects.

Entity:
    Animal

Function declaration:
    public void sound()
    public void run()
    public void run(int kilometer)

Jobs to be done:
     1) Declare the method sound.
        1.1)print the statement.
    2) Declare the method moves.
        2.1)print the statement.
    3) Declare the method moves with parameter 
        3.1)print the statement.
*/
package com.java.training.inheritance;

public class Animal {

    public void sound() {
        System.out.println("Sounds of animals");
    }

    public void move() {
        System.out.println("Animal is moving fast");
    }

    public void move(int kilometer) {
        System.out.println("Animal runs " + kilometer + " km without any tired");
    }
}
