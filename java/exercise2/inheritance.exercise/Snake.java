/*
Entity:
    Snake.

Function declaration:
    public void sound()

Jobs to be done:
    1.Declare the class Snake.
    2.Declare the method sound().
    3. Print the the statement.
*/
package com.java.training.inheritance;

public class Snake extends Animal {

    public void sound() {
        System.out.println("Snake crawls");
    }
}
