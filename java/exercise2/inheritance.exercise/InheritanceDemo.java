//main method
package com.java.training.inheritance;

public class InheritanceDemo {

    public static void main(String[] args) {
        Animal animal = new Animal();
        Dog dog = new Dog();
        Cat cat = new Cat();
        Snake snake = new Snake();
        animal.sound();
        animal.move();
        animal.move(7);
        cat.sound();
        cat.move();
        cat.move(6);
        dog.sound();
        dog.move();
        dog.move(8);
        snake.sound();
        snake.move();
        snake.move(9);
    }
}
