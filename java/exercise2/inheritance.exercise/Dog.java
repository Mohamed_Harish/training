/*
Entity:
    Dog

Function declaration:
    public void sound()

Jobs to be done:
    1.Declare the class Dog.
    2.Declare the method sound().
    3.Print the the statement.
*/
package com.java.training.inheritance;

public class Dog extends Animal {

    public void sound() {
        System.out.println("Dog barks");
    }
}
