import java.util.ArrayList;
import java.util.List;

public class WildCardDemo1 {    //Upper bounded wild card

    public static void main(String[] args) {
        List<Integer> rollNumberList = new ArrayList<Integer>();
        rollNumberList.add(3); 
        rollNumberList.add(5); 
        rollNumberList.add(10);
        print(rollNumberList);
            
        List<String> sectionList = new ArrayList<String>();
        sectionList.add("A"); 
        sectionList.add("B"); 
        sectionList.add("C");
        // print(sectionList); // LINE A
    }
    
    //public static void print(List<Number> identity) { // LINE B
    public static void print(List<? extends Number> identity) {// LINE C
        for(Number input : identity) {
            System.out.print(input +" ");
        }
    }
}