package com.java.training.wildcard;

import java.util.ArrayList;
import java.util.List;

public class WildCardDemo3 {

    public static void main(String[] args) {
        List<Integer> rollNumberList = new ArrayList<Integer>(); // LINE A

        addInteger(rollNumberList); // LINE B

        for (Object value : rollNumberList) {
            System.out.println(value);
        }
    }

    public static void addInteger(List<? super Integer> rollNumberList) {
        for (int number = 1; number < 5; number++) {
            rollNumberList.add(number);
        }
    }
}
