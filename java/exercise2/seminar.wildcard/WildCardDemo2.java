package com.java.training.wildcard;

import java.util.ArrayList;
import java.util.List;

public class WildCardDemo2 { // Unbounded wild card

    public static void main(String[] args) {
        List<Integer> rollNumberList = new ArrayList<Integer>();
        rollNumberList.add(6);
        rollNumberList.add(3);
        rollNumberList.add(10);
        print(rollNumberList);
        System.out.println("-------");
        List<String> sectionList = new ArrayList<String>();
        sectionList.add("A");
        sectionList.add("B");
        sectionList.add("C");
        print(sectionList);
    }

    public static void print(List<?> identity) {

        for (Object input : identity) {
            System.out.println(input + " ");
            // identity.add(input); // LINE A
        }
    }
}
