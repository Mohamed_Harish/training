/*
Requirement:
    To find the value of the following expression, and to say why
    Integer.valueOf(1).equals(Long.valueOf(1)).

Entity:
    ValueOfExpression

Function declaration:
    public static void main (String[] args)
    valueOf()

Jobs to be done:
    1. Print the given expression.
*/

/* Answer of the given expression is false. this is prooved in the below program. Because the two
   objects Integer and Long have different types.
*/

//Program:
package com.java.training.javadoc;

public class ValueOfExpression {

    public static void main(String[] args) {
        System.out.println(Integer.valueOf(1).equals(Long.valueOf(1)));
    }
}