/*
Requirement:
    To find the Integer method would be used to convert a string expressed
    in base 5 into the equivalent int

Entity:
    IntegerMethod

Function declaration:
    public static void main(String[] args)
    valueOf()

Jobs to be done:
    1. The  user input is initialized to the string variable.
    2. The string is converted into Integer type.
    3. The value of base 5 is converted to the Integer value.
*/
//Program:
package com.java.training.javadoc;

import java.util.Scanner;

public class IntegerMethod {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        int equivalentInteger = Integer.valueOf(string, 5);
        System.out.println("Equivalent integer value = " + " " + equivalentInteger);
        scanner.close();
    }
}
