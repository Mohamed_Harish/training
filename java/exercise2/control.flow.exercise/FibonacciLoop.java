/*
Requirement:
    To find the fibonacci series by using the for loop condition.

Entity:
    FibonacciLoop

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1)Declare two integer variable rangeValue and sum.
    2)Declare a integer variable number1 and assign 0 to it.
    3)Declare a integer variable number2 and assign 1 to it.
    4)Create a Scanner object.
    5)Get the integer input from user and assign it to rangeValue.
    6)For each value in the range 1 to rangeValue.
        6.1)add number1 and number2 and assign it to sum.
        6.2)assign the value of number2 to number1.
        6.3)assign the value of sum to number2.
        6.4)Print number1 and a white space.
*/
//Program:
package com.java.training.controlflow;

import java.util.Scanner;

public class FibonacciLoop {

    public static void main(String[] args) {
        int series;
        int number1 = 0;
        int number2 = 1;
        int sum;
        Scanner scanner = new Scanner(System.in);
        series = scanner.nextInt();
        for (int value = 1; value <= series; value++) {
            sum = number1 + number2;
            number1 = number2;
            number2 = sum;
            System.out.print(number1 + " ");
        }
        scanner.close();
    }
}
