/*
Requirement:
    To write the test program consisting the following codes snippet and make the value of aNumber
    to 3 and find the output of the code by using proper line space amd breakers and also by using
    proper curly braces. The following code snippet is given as
     if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
     else System.out.println("second string");
     System.out.println("third string");

Entity:
    Check

Function Declaration:
    public static void main(String[] args)

Jobs to be done:
    1)Declare the integer variable and assign 3 to it.
    2)Check whether the number is greater than or equal to 0.
        2.1)if true, then check whether the number is equal to 0.
            2.1.1)if true, then print "first string".
        2.2)if false, then print "second string".
    3)Print "third string".
*/
//Corrected program:
package com.java.training.controlflow;

import java.util.Scanner;

public class Check {

    public static void main(String[] args) {
        int aNumber;
        Scanner number = new Scanner(System.in);
        aNumber = number.nextInt();
        if (aNumber >= 0) {

            if (aNumber == 0) {
                System.out.println("first string");
            }
        } else {
            System.out.println("second string");
        }
        System.out.println("third string");
        number.close();
    }
}
