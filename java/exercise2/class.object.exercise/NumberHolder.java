/*
Requirement:
    To write a code that creates an instance of the class and initializes its two member variables
    with provided values,and then displays the value of each member variable.
    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }

Entity:
    NumberHolder

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1)Create a object of the class.
    2)Assign the values of two declared variables.
    3)Print the values of both the variables.
*/
//Corrected program:
package com.java.training.classobject;

public class NumberHolder {

    public int anInt;
    public float aFloat;

    public static void main(String[] args) {
        NumberHolder numberholder = new NumberHolder();
        numberholder.anInt = 5;
        numberholder.aFloat = 5.5f;
        System.out.println(numberholder.anInt);
        System.out.println(numberholder.aFloat);
    }
}
