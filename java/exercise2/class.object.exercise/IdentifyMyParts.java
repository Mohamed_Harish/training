/*
Requirement: 
    To identify the class and instance variable of the program.
        public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }

Entity:
    IdentifyMyParts

Function declaration:
    There is no function.

Jobs to be done:
    1)Declare a static integer variable x and assign the value 7.
    2)Declare a integer variable y and assign the value 3.


class variable: x
instance variable: y
*/