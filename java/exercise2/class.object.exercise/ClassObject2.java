/*
Requirement:
    To find out the output of the given code
        IdentifyMyParts a = new IdentifyMyParts();
        IdentifyMyParts b = new IdentifyMyParts();
        a.y = 5;
        b.y = 6;
        a.x = 1;
        b.x = 2;
        System.out.println("a.y = " + a.y);
        System.out.println("b.y = " + b.y);
        System.out.println("a.x = " + a.x);
        System.out.println("b.x = " + b.x);
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);

Entity:
    IdentifyMyParts

Function declaration:
    There is no function.

Jobs to be done:
    1)To consider the given program
    2)To Check the values of x & y in objects a and b.
    3)To print the resultant values of each variables.

OUTPUT:
a.y = 5
b.y = 6
a.x = 2
b.x = 2
IdentifyMyParts.x = 2
*/