package com.organization.rules;

public interface OrganizationRules {
    public void wearId();
    public void work();
    public void completeTask();
}