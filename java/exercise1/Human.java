// abstract class
public abstract class Human {
    // concreate method
    public void sleep() {
        System.out.println("Take rest!!");
    }
    // abstract method
    abstract public void work();
}

class Men extends Human {
    
    public void work() {
        System.out.println("Work for 8 hours");
    }
}

class Woman extends Human {

    public void work() {
        System.out.println("Work for 6 hours");
    }
}