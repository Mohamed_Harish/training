package com.library.book;//..creating package
import java.util.Scanner;
public class BookName {
    public static void refer(int id){//method signature
        System.out.println("Book id is " + id);
    }
    protected static void changeName(String name) {
        System.out.println("Provided to: " + name);
    }
    public static void main(String[] args) {
        BookName id = new BookName();
        Scanner sc = new Scanner(System.in);
        int no = sc.nextInt();
        id.refer(no);
    }
}