package com.department.myclass;
import java.util.Scanner;
import com.library.book.BookName;    //Fully qualified name

public class Section {

    private String sectionName;

    public int sectionCode;

    public Section() {
        //non-parameterized constructor
    }

    protected Section(int code, String name) {         //Parameterized constructor
        sectionCode = code;
        sectionName = name;
    }
    {
        sectionName = "A";
        sectionCode = 001;
    }
    public static void main(String[] args) {
        Scanner lend = new Scanner(System.in);
        int a = lend.nextInt();
        Section sect = new Section();
        System.out.println(" Section: " + sect.sectionName + " Section Code: " + sect.sectionCode);
        Section sect1 = new Section(002,"C");
        System.out.println(" Section: " + sect.sectionName + " Section Code: " + sect.sectionCode);
        System.out.println(" Section: " + sect1.sectionName + " Section Code: " + sect1.sectionCode);
        System.out.println("Lended Book id");
        com.library.book.BookName id = new com.library.book.BookName();
        id.refer(a);
    }
}