/*
Reference:
    To sort and print the strings alphabetically ignoring case. Also convert and print even indexed
    Strings into uppercase{ "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }
    
Entity:
    No Entity.
    
Function declaration:
    No function.
    
Jobs to be done:
    1.Consider the strings given .
    2.Sort the strings in alphabetical order.
    3.Print the sorted strings with even indexed string in uppercase.
*/
//Program:
import java.util.Arrays;
public class PlaceSort {
    public static void main(String[] args) {
        String [] place = { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        System.out.println("Given Array :" + Arrays.toString(place));
        Arrays.sort(place, String.CASE_INSENSITIVE_ORDER);
        for(int i = 0;i<place.length;i++){
            if(i%2 == 0) {
                place[i] = place[i].toUpperCase();
            }
        }
        System.out.println("Sorted Array :" + Arrays.toString(place));
    }
}