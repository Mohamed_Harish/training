import com.organization.rules.OrganizationRules;

public class Employee implements OrganizationRules {
    
    public void wearId() {
        System.out.println("Employees must wear identity card within the organization");
    }
    
    public void work() {
        System.out.println("Employees must work for 8 hours a day");
    }
    
    public void completeTask() {
        System.out.println("Tasks should be completed on time");
    }
    
    public void takeLeave() {
        System.out.println("3 days per month is permitted");
    }
    
    public static void main(String[] args) {
        Employee employee = new Employee();
        employee.wearId();
        employee.work();
        employee.completeTask();
        employee.takeLeave();
    }
}

