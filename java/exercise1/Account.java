//  outer class
public class Account {
    
    public long accNo = 153267894;
    public int age = 20;
    //  inner class
    public class Customer {
        
        public String firstName = "Mohamed";
        public String lastName = "Harish";
    }
}

